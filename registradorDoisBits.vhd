library IEEE;
use IEEE.Std_Logic_1164.all;
use ieee.std_logic_unsigned.all;

entity registradorDoisBits is 
	port (
			CLK: in std_logic;
			Enable: in std_logic;
			Reset: in std_logic;
			A: in std_logic_vector(1 downto 0);
			B: out std_logic_vector(1 downto 0)
			);
end registradorDoisBits;

architecture bhv of registradorDoisBits is
	begin
		process(CLK, Reset)
			begin			
				if Reset = '0' then
					B <= "00";
					
				elsif CLK'Event and CLK = '1' then
					if Enable = '1' then 
						B <= A;
					end if;
				end if;
		end process;
end bhv;
					