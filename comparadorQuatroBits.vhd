library IEEE;
use IEEE.Std_Logic_1164.all;

entity comparadorQuatroBits is
			port(
					comparingConstant: in std_logic_vector(3 downto 0);
					comparingVariable: in std_logic_vector(3 downto 0);
					result: out std_logic
				 );
end comparadorQuatroBits;

architecture behaviour of comparadorQuatroBits is
		begin
			result <= '1' when comparingVariable = comparingConstant else
			'0';
end behaviour;
		