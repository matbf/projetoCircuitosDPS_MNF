-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "11/27/2017 16:30:02"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	awesomeGame IS
    PORT (
	CLOCK_50 : IN std_logic;
	KEY : IN std_logic_vector(3 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0);
	HEX0 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX1 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX3 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX2 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX4 : BUFFER std_logic_vector(6 DOWNTO 0);
	HEX5 : BUFFER std_logic_vector(6 DOWNTO 0);
	LEDR : BUFFER std_logic_vector(9 DOWNTO 0)
	);
END awesomeGame;

-- Design Ports Information
-- HEX0[0]	=>  Location: PIN_AE26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_AE27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_AE28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_AG27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_AF28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_AG28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_AH28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_AJ29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_AH29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_AH30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_AG30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_AF29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_AF30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_AD27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_AD26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_AC27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_AD25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_AC25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_AB25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_AB23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_AE29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_AD29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_AC28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_AD30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_AC29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_AC30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[0]	=>  Location: PIN_AA24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[1]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[2]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[3]	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[4]	=>  Location: PIN_W24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[5]	=>  Location: PIN_V23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[6]	=>  Location: PIN_W25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[0]	=>  Location: PIN_V25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[1]	=>  Location: PIN_AA28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[2]	=>  Location: PIN_Y27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[3]	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[4]	=>  Location: PIN_AB26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[5]	=>  Location: PIN_AA26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[6]	=>  Location: PIN_AA25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_W20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_AD12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_AE11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_AC9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AF10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_AD11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_AC12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_AF9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_AE12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_AD10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF awesomeGame IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX4 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX5 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~101_sumout\ : std_logic;
SIGNAL \passeData|contaIteracoes|Iteration[0]~2_combout\ : std_logic;
SIGNAL \control|EA.E1~0_combout\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \L1|btn0state.EsperaApertar~0_combout\ : std_logic;
SIGNAL \L1|btn0state.EsperaApertar~q\ : std_logic;
SIGNAL \L1|btn0next.SaidaAtiva~0_combout\ : std_logic;
SIGNAL \L1|btn0state.SaidaAtiva~q\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \L1|btn1state.EsperaApertar~0_combout\ : std_logic;
SIGNAL \L1|btn1state.EsperaApertar~q\ : std_logic;
SIGNAL \L1|btn1next.SaidaAtiva~0_combout\ : std_logic;
SIGNAL \L1|btn1state.SaidaAtiva~q\ : std_logic;
SIGNAL \control|EA.E1~q\ : std_logic;
SIGNAL \passeData|a3|Mux2~0_combout\ : std_logic;
SIGNAL \passeData|contaIteracoes|Iteration[3]~0_combout\ : std_logic;
SIGNAL \passeData|compDez|Equal0~0_combout\ : std_logic;
SIGNAL \control|Selector2~0_combout\ : std_logic;
SIGNAL \control|EA.E2~q\ : std_logic;
SIGNAL \control|Selector3~0_combout\ : std_logic;
SIGNAL \control|EA.E3~q\ : std_logic;
SIGNAL \control|EA.E0~0_combout\ : std_logic;
SIGNAL \control|EA.E0~q\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~9_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~8_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~0_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|Equal2~1_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|Equal2~2_combout\ : std_logic;
SIGNAL \passeData|tempo|Op[26]~0_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~10_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~0_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~7_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~5_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~6_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~1_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~2_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~5_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~6_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~3_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~3_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~1_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~2_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~4_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|LessThan0~11_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~102\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~97_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~98\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~89_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~90\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~81_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Iteration[3]~feeder_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~82\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~109_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~110\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~113_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~114\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~105_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~106\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~77_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~78\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~125_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~126\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~93_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~94\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~117_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~118\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~85_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~86\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~121_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~122\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~29_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~30\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~41_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~42\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~45_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~46\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~33_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~34\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~37_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~38\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~49_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~50\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~57_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~58\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~61_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~62\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~53_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~54\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~25_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~26\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~21_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~22\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~17_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~18\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~1_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~2\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~5_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~6\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~9_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~10\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~13_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~14\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~65_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~66\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~73_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~74\ : std_logic;
SIGNAL \passeData|ClockTempo|Add0~69_sumout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~7_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~10_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~8_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~9_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~11_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~4_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|Equal0~combout\ : std_logic;
SIGNAL \passeData|contaIteracoes|Iteration[2]~1_combout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[1]~0_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[3]~1_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[0]~2_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[2]~3_combout\ : std_logic;
SIGNAL \passeData|motraCasaZeroJogo|F[0]~0_combout\ : std_logic;
SIGNAL \passeData|motraCasaZeroJogo|F[1]~1_combout\ : std_logic;
SIGNAL \passeData|motraCasaZeroJogo|F[2]~2_combout\ : std_logic;
SIGNAL \passeData|motraCasaZeroJogo|F[3]~3_combout\ : std_logic;
SIGNAL \passeData|motraCasaZeroJogo|F[4]~4_combout\ : std_logic;
SIGNAL \passeData|motraCasaZeroJogo|F[5]~5_combout\ : std_logic;
SIGNAL \passeData|motraCasaZeroJogo|F[6]~6_combout\ : std_logic;
SIGNAL \passeData|a2|Mux2~0_combout\ : std_logic;
SIGNAL \passeData|a4|Mux2~0_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[5]~6_combout\ : std_logic;
SIGNAL \passeData|a3|Mux1~0_combout\ : std_logic;
SIGNAL \passeData|a2|Mux1~0_combout\ : std_logic;
SIGNAL \passeData|a4|Mux1~0_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[6]~7_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[4]~5_combout\ : std_logic;
SIGNAL \passeData|a3|Mux0~0_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[7]~4_combout\ : std_logic;
SIGNAL \passeData|mostraCasaUmJogo|F[0]~0_combout\ : std_logic;
SIGNAL \passeData|mostraCasaUmJogo|F[1]~1_combout\ : std_logic;
SIGNAL \passeData|mostraCasaUmJogo|F[2]~2_combout\ : std_logic;
SIGNAL \passeData|mostraCasaUmJogo|F[3]~3_combout\ : std_logic;
SIGNAL \passeData|mostraCasaUmJogo|F[4]~4_combout\ : std_logic;
SIGNAL \passeData|mostraCasaUmJogo|F[5]~5_combout\ : std_logic;
SIGNAL \passeData|mostraCasaUmJogo|F[6]~6_combout\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \passeData|ChecaAcerto|Equal0~3_combout\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \passeData|ChecaAcerto|Equal0~2_combout\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \passeData|ChecaAcerto|Equal0~1_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|Op[5]~8_combout\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \passeData|ChecaAcerto|Equal0~0_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|Equal2~0_combout\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \passeData|ChecaAcerto|Equal0~4_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|pontuacao[1]~2_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|pontuacao[2]~3_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|pontuacao[3]~0_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|pontuacao[3]~1_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|pontuacao[0]~4_combout\ : std_logic;
SIGNAL \passeData|pontuacaoCasaZero|F[0]~0_combout\ : std_logic;
SIGNAL \passeData|pontuacaoCasaZero|F[1]~1_combout\ : std_logic;
SIGNAL \passeData|pontuacaoCasaZero|F[2]~2_combout\ : std_logic;
SIGNAL \passeData|pontuacaoCasaZero|F[3]~3_combout\ : std_logic;
SIGNAL \passeData|pontuacaoCasaZero|F[4]~4_combout\ : std_logic;
SIGNAL \passeData|pontuacaoCasaZero|F[5]~5_combout\ : std_logic;
SIGNAL \passeData|pontuacaoCasaZero|F[6]~6_combout\ : std_logic;
SIGNAL \passeData|CasaNivel|Equal13~0_combout\ : std_logic;
SIGNAL \passeData|CasaNivel|F[2]~0_combout\ : std_logic;
SIGNAL \passeData|CasaNivel|F[5]~1_combout\ : std_logic;
SIGNAL \passeData|regL|B[0]~feeder_combout\ : std_logic;
SIGNAL \passeData|regL|B[1]~feeder_combout\ : std_logic;
SIGNAL \passeData|regL|B[2]~feeder_combout\ : std_logic;
SIGNAL \passeData|regL|B[6]~feeder_combout\ : std_logic;
SIGNAL \passeData|Mux19~0_combout\ : std_logic;
SIGNAL \passeData|ChecaAcerto|Equal0~5_combout\ : std_logic;
SIGNAL \passeData|Mux18~0_combout\ : std_logic;
SIGNAL \passeData|Leds0~combout\ : std_logic;
SIGNAL \passeData|Mux16~0_combout\ : std_logic;
SIGNAL \passeData|Mux17~0_combout\ : std_logic;
SIGNAL \passeData|Leds1~combout\ : std_logic;
SIGNAL \passeData|Mux15~0_combout\ : std_logic;
SIGNAL \passeData|Mux14~0_combout\ : std_logic;
SIGNAL \passeData|Leds2~combout\ : std_logic;
SIGNAL \passeData|Mux12~0_combout\ : std_logic;
SIGNAL \passeData|Mux13~0_combout\ : std_logic;
SIGNAL \passeData|Leds3~combout\ : std_logic;
SIGNAL \passeData|Mux11~0_combout\ : std_logic;
SIGNAL \passeData|Mux10~0_combout\ : std_logic;
SIGNAL \passeData|Leds4~combout\ : std_logic;
SIGNAL \passeData|Mux9~0_combout\ : std_logic;
SIGNAL \passeData|Mux8~0_combout\ : std_logic;
SIGNAL \passeData|Leds5~combout\ : std_logic;
SIGNAL \passeData|Mux6~0_combout\ : std_logic;
SIGNAL \passeData|Mux7~0_combout\ : std_logic;
SIGNAL \passeData|Leds6~combout\ : std_logic;
SIGNAL \passeData|Mux5~0_combout\ : std_logic;
SIGNAL \passeData|Mux4~0_combout\ : std_logic;
SIGNAL \passeData|Leds7~combout\ : std_logic;
SIGNAL \passeData|Mux2~0_combout\ : std_logic;
SIGNAL \passeData|Mux3~0_combout\ : std_logic;
SIGNAL \passeData|Leds8~combout\ : std_logic;
SIGNAL \passeData|Mux1~0_combout\ : std_logic;
SIGNAL \passeData|Mux0~0_combout\ : std_logic;
SIGNAL \passeData|Leds9~combout\ : std_logic;
SIGNAL \passeData|registradorMemoria|B\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \passeData|mostraResultado|B\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \passeData|ClockTempo|Iteration\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \passeData|regNivel|B\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \passeData|mostraRegUm|B\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \passeData|mostraRegZero|B\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \passeData|regL|B\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \passeData|contaIteracoes|Iteration\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \passeData|registradorNivel|B\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \passeData|a4|ALT_INV_Mux2~0_combout\ : std_logic;
SIGNAL \passeData|a2|ALT_INV_Mux2~0_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\ : std_logic;
SIGNAL \passeData|compDez|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \passeData|a3|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \control|ALT_INV_EA.E2~q\ : std_logic;
SIGNAL \control|ALT_INV_EA.E0~q\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\ : std_logic;
SIGNAL \passeData|registradorMemoria|ALT_INV_B\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \passeData|contaIteracoes|ALT_INV_Iteration\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \passeData|CasaNivel|ALT_INV_F[2]~0_combout\ : std_logic;
SIGNAL \passeData|regNivel|ALT_INV_B\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \passeData|pontuacaoCasaZero|ALT_INV_F[6]~6_combout\ : std_logic;
SIGNAL \passeData|mostraResultado|ALT_INV_B\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \passeData|ClockTempo|ALT_INV_Add0~81_sumout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_Iteration\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \ALT_INV_CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \ALT_INV_KEY[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[5]~input_o\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds9~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds8~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds7~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds6~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds5~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds4~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds3~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds2~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds1~combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Leds0~combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~10_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~9_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~8_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~7_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~6_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~5_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~4_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~3_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \L1|ALT_INV_btn1state.EsperaApertar~q\ : std_logic;
SIGNAL \L1|ALT_INV_btn0state.EsperaApertar~q\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~11_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~10_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~9_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~8_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~7_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~6_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~5_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~4_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~3_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \passeData|ClockTempo|ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \passeData|tempo|ALT_INV_Op[26]~0_combout\ : std_logic;
SIGNAL \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \L1|ALT_INV_btn1state.SaidaAtiva~q\ : std_logic;
SIGNAL \L1|ALT_INV_btn0state.SaidaAtiva~q\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux3~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux2~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux5~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux4~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux7~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux9~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux8~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux11~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux10~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux13~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux12~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux15~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux14~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux17~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux16~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux19~0_combout\ : std_logic;
SIGNAL \passeData|ALT_INV_Mux18~0_combout\ : std_logic;
SIGNAL \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\ : std_logic;
SIGNAL \control|ALT_INV_EA.E1~q\ : std_logic;
SIGNAL \passeData|resultadoUltimo|ALT_INV_Equal2~2_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|ALT_INV_Equal2~1_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|ALT_INV_Equal2~0_combout\ : std_logic;
SIGNAL \control|ALT_INV_EA.E3~q\ : std_logic;
SIGNAL \passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\ : std_logic;
SIGNAL \passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\ : std_logic;
SIGNAL \passeData|resultadoUltimo|ALT_INV_pontuacao[3]~0_combout\ : std_logic;
SIGNAL \passeData|registradorNivel|ALT_INV_B\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[5]~8_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\ : std_logic;
SIGNAL \passeData|a2|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \passeData|a4|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \passeData|a3|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\ : std_logic;
SIGNAL \passeData|a3|ALT_INV_Mux2~0_combout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_KEY <= KEY;
ww_SW <= SW;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX3 <= ww_HEX3;
HEX2 <= ww_HEX2;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\passeData|a4|ALT_INV_Mux2~0_combout\ <= NOT \passeData|a4|Mux2~0_combout\;
\passeData|a2|ALT_INV_Mux2~0_combout\ <= NOT \passeData|a2|Mux2~0_combout\;
\passeData|encontraResposta|ALT_INV_Op[4]~5_combout\ <= NOT \passeData|encontraResposta|Op[4]~5_combout\;
\passeData|encontraResposta|ALT_INV_Op[7]~4_combout\ <= NOT \passeData|encontraResposta|Op[7]~4_combout\;
\passeData|compDez|ALT_INV_Equal0~0_combout\ <= NOT \passeData|compDez|Equal0~0_combout\;
\passeData|a3|ALT_INV_Mux0~0_combout\ <= NOT \passeData|a3|Mux0~0_combout\;
\control|ALT_INV_EA.E2~q\ <= NOT \control|EA.E2~q\;
\control|ALT_INV_EA.E0~q\ <= NOT \control|EA.E0~q\;
\passeData|encontraResposta|ALT_INV_Op[2]~3_combout\ <= NOT \passeData|encontraResposta|Op[2]~3_combout\;
\passeData|encontraResposta|ALT_INV_Op[0]~2_combout\ <= NOT \passeData|encontraResposta|Op[0]~2_combout\;
\passeData|encontraResposta|ALT_INV_Op[3]~1_combout\ <= NOT \passeData|encontraResposta|Op[3]~1_combout\;
\passeData|encontraResposta|ALT_INV_Op[1]~0_combout\ <= NOT \passeData|encontraResposta|Op[1]~0_combout\;
\passeData|registradorMemoria|ALT_INV_B\(1) <= NOT \passeData|registradorMemoria|B\(1);
\passeData|contaIteracoes|ALT_INV_Iteration\(0) <= NOT \passeData|contaIteracoes|Iteration\(0);
\passeData|contaIteracoes|ALT_INV_Iteration\(1) <= NOT \passeData|contaIteracoes|Iteration\(1);
\passeData|contaIteracoes|ALT_INV_Iteration\(2) <= NOT \passeData|contaIteracoes|Iteration\(2);
\passeData|contaIteracoes|ALT_INV_Iteration\(3) <= NOT \passeData|contaIteracoes|Iteration\(3);
\passeData|registradorMemoria|ALT_INV_B\(0) <= NOT \passeData|registradorMemoria|B\(0);
\passeData|CasaNivel|ALT_INV_F[2]~0_combout\ <= NOT \passeData|CasaNivel|F[2]~0_combout\;
\passeData|regNivel|ALT_INV_B\(0) <= NOT \passeData|regNivel|B\(0);
\passeData|regNivel|ALT_INV_B\(1) <= NOT \passeData|regNivel|B\(1);
\passeData|pontuacaoCasaZero|ALT_INV_F[6]~6_combout\ <= NOT \passeData|pontuacaoCasaZero|F[6]~6_combout\;
\passeData|mostraResultado|ALT_INV_B\(0) <= NOT \passeData|mostraResultado|B\(0);
\passeData|mostraResultado|ALT_INV_B\(2) <= NOT \passeData|mostraResultado|B\(2);
\passeData|mostraResultado|ALT_INV_B\(1) <= NOT \passeData|mostraResultado|B\(1);
\passeData|mostraResultado|ALT_INV_B\(3) <= NOT \passeData|mostraResultado|B\(3);
\passeData|ClockTempo|ALT_INV_Add0~81_sumout\ <= NOT \passeData|ClockTempo|Add0~81_sumout\;
\passeData|ClockTempo|ALT_INV_Iteration\(8) <= NOT \passeData|ClockTempo|Iteration\(8);
\passeData|ClockTempo|ALT_INV_Iteration\(12) <= NOT \passeData|ClockTempo|Iteration\(12);
\passeData|ClockTempo|ALT_INV_Iteration\(10) <= NOT \passeData|ClockTempo|Iteration\(10);
\passeData|ClockTempo|ALT_INV_Iteration\(5) <= NOT \passeData|ClockTempo|Iteration\(5);
\passeData|ClockTempo|ALT_INV_Iteration\(4) <= NOT \passeData|ClockTempo|Iteration\(4);
\passeData|ClockTempo|ALT_INV_Iteration\(6) <= NOT \passeData|ClockTempo|Iteration\(6);
\passeData|ClockTempo|ALT_INV_Iteration\(0) <= NOT \passeData|ClockTempo|Iteration\(0);
\passeData|ClockTempo|ALT_INV_Iteration\(1) <= NOT \passeData|ClockTempo|Iteration\(1);
\passeData|ClockTempo|ALT_INV_Iteration\(9) <= NOT \passeData|ClockTempo|Iteration\(9);
\passeData|ClockTempo|ALT_INV_Iteration\(2) <= NOT \passeData|ClockTempo|Iteration\(2);
\passeData|ClockTempo|ALT_INV_Iteration\(11) <= NOT \passeData|ClockTempo|Iteration\(11);
\passeData|ClockTempo|ALT_INV_Iteration\(3) <= NOT \passeData|ClockTempo|Iteration\(3);
\passeData|ClockTempo|ALT_INV_Iteration\(7) <= NOT \passeData|ClockTempo|Iteration\(7);
\passeData|ClockTempo|ALT_INV_Iteration\(30) <= NOT \passeData|ClockTempo|Iteration\(30);
\passeData|ClockTempo|ALT_INV_Iteration\(31) <= NOT \passeData|ClockTempo|Iteration\(31);
\passeData|ClockTempo|ALT_INV_Iteration\(29) <= NOT \passeData|ClockTempo|Iteration\(29);
\passeData|ClockTempo|ALT_INV_Iteration\(20) <= NOT \passeData|ClockTempo|Iteration\(20);
\passeData|ClockTempo|ALT_INV_Iteration\(19) <= NOT \passeData|ClockTempo|Iteration\(19);
\passeData|ClockTempo|ALT_INV_Iteration\(21) <= NOT \passeData|ClockTempo|Iteration\(21);
\passeData|ClockTempo|ALT_INV_Iteration\(18) <= NOT \passeData|ClockTempo|Iteration\(18);
\passeData|ClockTempo|ALT_INV_Iteration\(15) <= NOT \passeData|ClockTempo|Iteration\(15);
\passeData|ClockTempo|ALT_INV_Iteration\(14) <= NOT \passeData|ClockTempo|Iteration\(14);
\passeData|ClockTempo|ALT_INV_Iteration\(17) <= NOT \passeData|ClockTempo|Iteration\(17);
\passeData|ClockTempo|ALT_INV_Iteration\(16) <= NOT \passeData|ClockTempo|Iteration\(16);
\passeData|ClockTempo|ALT_INV_Iteration\(13) <= NOT \passeData|ClockTempo|Iteration\(13);
\passeData|ClockTempo|ALT_INV_Iteration\(22) <= NOT \passeData|ClockTempo|Iteration\(22);
\passeData|ClockTempo|ALT_INV_Iteration\(23) <= NOT \passeData|ClockTempo|Iteration\(23);
\passeData|ClockTempo|ALT_INV_Iteration\(24) <= NOT \passeData|ClockTempo|Iteration\(24);
\passeData|ClockTempo|ALT_INV_Iteration\(28) <= NOT \passeData|ClockTempo|Iteration\(28);
\passeData|ClockTempo|ALT_INV_Iteration\(27) <= NOT \passeData|ClockTempo|Iteration\(27);
\passeData|ClockTempo|ALT_INV_Iteration\(26) <= NOT \passeData|ClockTempo|Iteration\(26);
\passeData|ClockTempo|ALT_INV_Iteration\(25) <= NOT \passeData|ClockTempo|Iteration\(25);
\ALT_INV_CLOCK_50~inputCLKENA0_outclk\ <= NOT \CLOCK_50~inputCLKENA0_outclk\;
\ALT_INV_KEY[1]~input_o\ <= NOT \KEY[1]~input_o\;
\ALT_INV_KEY[0]~input_o\ <= NOT \KEY[0]~input_o\;
\ALT_INV_SW[2]~input_o\ <= NOT \SW[2]~input_o\;
\ALT_INV_SW[1]~input_o\ <= NOT \SW[1]~input_o\;
\ALT_INV_SW[0]~input_o\ <= NOT \SW[0]~input_o\;
\ALT_INV_SW[4]~input_o\ <= NOT \SW[4]~input_o\;
\ALT_INV_SW[3]~input_o\ <= NOT \SW[3]~input_o\;
\ALT_INV_SW[7]~input_o\ <= NOT \SW[7]~input_o\;
\ALT_INV_SW[6]~input_o\ <= NOT \SW[6]~input_o\;
\ALT_INV_SW[5]~input_o\ <= NOT \SW[5]~input_o\;
\passeData|ALT_INV_Leds9~combout\ <= NOT \passeData|Leds9~combout\;
\passeData|ALT_INV_Leds8~combout\ <= NOT \passeData|Leds8~combout\;
\passeData|ALT_INV_Leds7~combout\ <= NOT \passeData|Leds7~combout\;
\passeData|ALT_INV_Leds6~combout\ <= NOT \passeData|Leds6~combout\;
\passeData|ALT_INV_Leds5~combout\ <= NOT \passeData|Leds5~combout\;
\passeData|ALT_INV_Leds4~combout\ <= NOT \passeData|Leds4~combout\;
\passeData|ALT_INV_Leds3~combout\ <= NOT \passeData|Leds3~combout\;
\passeData|ALT_INV_Leds2~combout\ <= NOT \passeData|Leds2~combout\;
\passeData|ALT_INV_Leds1~combout\ <= NOT \passeData|Leds1~combout\;
\passeData|ALT_INV_Leds0~combout\ <= NOT \passeData|Leds0~combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~10_combout\ <= NOT \passeData|ClockTempo|LessThan0~10_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~9_combout\ <= NOT \passeData|ClockTempo|LessThan0~9_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~8_combout\ <= NOT \passeData|ClockTempo|LessThan0~8_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~7_combout\ <= NOT \passeData|ClockTempo|LessThan0~7_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~6_combout\ <= NOT \passeData|ClockTempo|LessThan0~6_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~5_combout\ <= NOT \passeData|ClockTempo|LessThan0~5_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~4_combout\ <= NOT \passeData|ClockTempo|LessThan0~4_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~3_combout\ <= NOT \passeData|ClockTempo|LessThan0~3_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~2_combout\ <= NOT \passeData|ClockTempo|LessThan0~2_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~1_combout\ <= NOT \passeData|ClockTempo|LessThan0~1_combout\;
\L1|ALT_INV_btn1state.EsperaApertar~q\ <= NOT \L1|btn1state.EsperaApertar~q\;
\L1|ALT_INV_btn0state.EsperaApertar~q\ <= NOT \L1|btn0state.EsperaApertar~q\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~11_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~11_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~10_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~10_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~9_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~9_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~8_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~8_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~7_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~7_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~6_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~6_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~5_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~5_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~4_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~4_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~3_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~3_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~2_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~2_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~1_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~1_combout\;
\passeData|ClockTempo|ALT_INV_LessThan0~0_combout\ <= NOT \passeData|ClockTempo|LessThan0~0_combout\;
\passeData|tempo|ALT_INV_Op[26]~0_combout\ <= NOT \passeData|tempo|Op[26]~0_combout\;
\passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~0_combout\ <= NOT \passeData|ChecaSeLimiteTempo|Equal0~0_combout\;
\L1|ALT_INV_btn1state.SaidaAtiva~q\ <= NOT \L1|btn1state.SaidaAtiva~q\;
\L1|ALT_INV_btn0state.SaidaAtiva~q\ <= NOT \L1|btn0state.SaidaAtiva~q\;
\passeData|ALT_INV_Mux1~0_combout\ <= NOT \passeData|Mux1~0_combout\;
\passeData|ALT_INV_Mux0~0_combout\ <= NOT \passeData|Mux0~0_combout\;
\passeData|ALT_INV_Mux3~0_combout\ <= NOT \passeData|Mux3~0_combout\;
\passeData|ALT_INV_Mux2~0_combout\ <= NOT \passeData|Mux2~0_combout\;
\passeData|ALT_INV_Mux5~0_combout\ <= NOT \passeData|Mux5~0_combout\;
\passeData|ALT_INV_Mux4~0_combout\ <= NOT \passeData|Mux4~0_combout\;
\passeData|ALT_INV_Mux7~0_combout\ <= NOT \passeData|Mux7~0_combout\;
\passeData|ALT_INV_Mux6~0_combout\ <= NOT \passeData|Mux6~0_combout\;
\passeData|ALT_INV_Mux9~0_combout\ <= NOT \passeData|Mux9~0_combout\;
\passeData|ALT_INV_Mux8~0_combout\ <= NOT \passeData|Mux8~0_combout\;
\passeData|ALT_INV_Mux11~0_combout\ <= NOT \passeData|Mux11~0_combout\;
\passeData|ALT_INV_Mux10~0_combout\ <= NOT \passeData|Mux10~0_combout\;
\passeData|ALT_INV_Mux13~0_combout\ <= NOT \passeData|Mux13~0_combout\;
\passeData|ALT_INV_Mux12~0_combout\ <= NOT \passeData|Mux12~0_combout\;
\passeData|ALT_INV_Mux15~0_combout\ <= NOT \passeData|Mux15~0_combout\;
\passeData|ALT_INV_Mux14~0_combout\ <= NOT \passeData|Mux14~0_combout\;
\passeData|ALT_INV_Mux17~0_combout\ <= NOT \passeData|Mux17~0_combout\;
\passeData|ALT_INV_Mux16~0_combout\ <= NOT \passeData|Mux16~0_combout\;
\passeData|ALT_INV_Mux19~0_combout\ <= NOT \passeData|Mux19~0_combout\;
\passeData|ALT_INV_Mux18~0_combout\ <= NOT \passeData|Mux18~0_combout\;
\passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\ <= NOT \passeData|ChecaAcerto|Equal0~5_combout\;
\control|ALT_INV_EA.E1~q\ <= NOT \control|EA.E1~q\;
\passeData|resultadoUltimo|ALT_INV_Equal2~2_combout\ <= NOT \passeData|resultadoUltimo|Equal2~2_combout\;
\passeData|resultadoUltimo|ALT_INV_Equal2~1_combout\ <= NOT \passeData|resultadoUltimo|Equal2~1_combout\;
\passeData|resultadoUltimo|ALT_INV_Equal2~0_combout\ <= NOT \passeData|resultadoUltimo|Equal2~0_combout\;
\control|ALT_INV_EA.E3~q\ <= NOT \control|EA.E3~q\;
\passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\ <= NOT \passeData|ChecaAcerto|Equal0~4_combout\;
\passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\ <= NOT \passeData|ChecaAcerto|Equal0~3_combout\;
\passeData|resultadoUltimo|ALT_INV_pontuacao[3]~0_combout\ <= NOT \passeData|resultadoUltimo|pontuacao[3]~0_combout\;
\passeData|registradorNivel|ALT_INV_B\(0) <= NOT \passeData|registradorNivel|B\(0);
\passeData|registradorNivel|ALT_INV_B\(1) <= NOT \passeData|registradorNivel|B\(1);
\passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\ <= NOT \passeData|ChecaAcerto|Equal0~2_combout\;
\passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\ <= NOT \passeData|ChecaAcerto|Equal0~1_combout\;
\passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\ <= NOT \passeData|ChecaAcerto|Equal0~0_combout\;
\passeData|encontraResposta|ALT_INV_Op[5]~8_combout\ <= NOT \passeData|encontraResposta|Op[5]~8_combout\;
\passeData|encontraResposta|ALT_INV_Op[6]~7_combout\ <= NOT \passeData|encontraResposta|Op[6]~7_combout\;
\passeData|a2|ALT_INV_Mux1~0_combout\ <= NOT \passeData|a2|Mux1~0_combout\;
\passeData|a4|ALT_INV_Mux1~0_combout\ <= NOT \passeData|a4|Mux1~0_combout\;
\passeData|a3|ALT_INV_Mux1~0_combout\ <= NOT \passeData|a3|Mux1~0_combout\;
\passeData|encontraResposta|ALT_INV_Op[5]~6_combout\ <= NOT \passeData|encontraResposta|Op[5]~6_combout\;
\passeData|a3|ALT_INV_Mux2~0_combout\ <= NOT \passeData|a3|Mux2~0_combout\;

-- Location: IOOBUF_X89_Y8_N39
\HEX0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegZero|B\(0),
	devoe => ww_devoe,
	o => ww_HEX0(0));

-- Location: IOOBUF_X89_Y11_N79
\HEX0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegZero|B\(1),
	devoe => ww_devoe,
	o => ww_HEX0(1));

-- Location: IOOBUF_X89_Y11_N96
\HEX0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegZero|B\(2),
	devoe => ww_devoe,
	o => ww_HEX0(2));

-- Location: IOOBUF_X89_Y4_N79
\HEX0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegZero|B\(3),
	devoe => ww_devoe,
	o => ww_HEX0(3));

-- Location: IOOBUF_X89_Y13_N56
\HEX0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegZero|B\(4),
	devoe => ww_devoe,
	o => ww_HEX0(4));

-- Location: IOOBUF_X89_Y13_N39
\HEX0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegZero|B\(5),
	devoe => ww_devoe,
	o => ww_HEX0(5));

-- Location: IOOBUF_X89_Y4_N96
\HEX0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegZero|B\(6),
	devoe => ww_devoe,
	o => ww_HEX0(6));

-- Location: IOOBUF_X89_Y6_N39
\HEX1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegUm|B\(0),
	devoe => ww_devoe,
	o => ww_HEX1(0));

-- Location: IOOBUF_X89_Y6_N56
\HEX1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegUm|B\(1),
	devoe => ww_devoe,
	o => ww_HEX1(1));

-- Location: IOOBUF_X89_Y16_N39
\HEX1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegUm|B\(2),
	devoe => ww_devoe,
	o => ww_HEX1(2));

-- Location: IOOBUF_X89_Y16_N56
\HEX1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegUm|B\(3),
	devoe => ww_devoe,
	o => ww_HEX1(3));

-- Location: IOOBUF_X89_Y15_N39
\HEX1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegUm|B\(4),
	devoe => ww_devoe,
	o => ww_HEX1(4));

-- Location: IOOBUF_X89_Y15_N56
\HEX1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegUm|B\(5),
	devoe => ww_devoe,
	o => ww_HEX1(5));

-- Location: IOOBUF_X89_Y8_N56
\HEX1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|mostraRegUm|B\(6),
	devoe => ww_devoe,
	o => ww_HEX1(6));

-- Location: IOOBUF_X89_Y16_N5
\HEX3[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(0));

-- Location: IOOBUF_X89_Y16_N22
\HEX3[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(1));

-- Location: IOOBUF_X89_Y4_N45
\HEX3[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(2));

-- Location: IOOBUF_X89_Y4_N62
\HEX3[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(3));

-- Location: IOOBUF_X89_Y21_N39
\HEX3[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(4));

-- Location: IOOBUF_X89_Y11_N62
\HEX3[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX3(5));

-- Location: IOOBUF_X89_Y9_N5
\HEX3[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_HEX3(6));

-- Location: IOOBUF_X89_Y9_N22
\HEX2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|pontuacaoCasaZero|F[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(0));

-- Location: IOOBUF_X89_Y23_N39
\HEX2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|pontuacaoCasaZero|F[1]~1_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(1));

-- Location: IOOBUF_X89_Y23_N56
\HEX2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|pontuacaoCasaZero|F[2]~2_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(2));

-- Location: IOOBUF_X89_Y20_N79
\HEX2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|pontuacaoCasaZero|F[3]~3_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(3));

-- Location: IOOBUF_X89_Y25_N39
\HEX2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|pontuacaoCasaZero|F[4]~4_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(4));

-- Location: IOOBUF_X89_Y20_N96
\HEX2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|pontuacaoCasaZero|F[5]~5_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(5));

-- Location: IOOBUF_X89_Y25_N56
\HEX2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|pontuacaoCasaZero|ALT_INV_F[6]~6_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(6));

-- Location: IOOBUF_X89_Y11_N45
\HEX4[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|CasaNivel|Equal13~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(0));

-- Location: IOOBUF_X89_Y13_N5
\HEX4[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX4(1));

-- Location: IOOBUF_X89_Y13_N22
\HEX4[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|CasaNivel|ALT_INV_F[2]~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(2));

-- Location: IOOBUF_X89_Y8_N22
\HEX4[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|CasaNivel|Equal13~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(3));

-- Location: IOOBUF_X89_Y15_N22
\HEX4[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|regNivel|B\(0),
	devoe => ww_devoe,
	o => ww_HEX4(4));

-- Location: IOOBUF_X89_Y15_N5
\HEX4[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|CasaNivel|F[5]~1_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(5));

-- Location: IOOBUF_X89_Y20_N45
\HEX4[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|regNivel|ALT_INV_B\(1),
	devoe => ww_devoe,
	o => ww_HEX4(6));

-- Location: IOOBUF_X89_Y20_N62
\HEX5[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|regL|B\(0),
	devoe => ww_devoe,
	o => ww_HEX5(0));

-- Location: IOOBUF_X89_Y21_N56
\HEX5[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|regL|B\(1),
	devoe => ww_devoe,
	o => ww_HEX5(1));

-- Location: IOOBUF_X89_Y25_N22
\HEX5[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|regL|B\(2),
	devoe => ww_devoe,
	o => ww_HEX5(2));

-- Location: IOOBUF_X89_Y23_N22
\HEX5[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(3));

-- Location: IOOBUF_X89_Y9_N56
\HEX5[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(4));

-- Location: IOOBUF_X89_Y23_N5
\HEX5[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_HEX5(5));

-- Location: IOOBUF_X89_Y9_N39
\HEX5[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|regL|B\(6),
	devoe => ww_devoe,
	o => ww_HEX5(6));

-- Location: IOOBUF_X52_Y0_N2
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds0~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N19
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds1~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X60_Y0_N2
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds2~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X80_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds3~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X60_Y0_N19
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds4~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X80_Y0_N19
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds5~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X84_Y0_N2
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds6~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X89_Y6_N5
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds7~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X89_Y8_N5
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds8~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X89_Y6_N22
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \passeData|Leds9~combout\,
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: CLKCTRL_G6
\CLOCK_50~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~input_o\,
	outclk => \CLOCK_50~inputCLKENA0_outclk\);

-- Location: LABCELL_X85_Y13_N0
\passeData|ClockTempo|Add0~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~101_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(0) ) + ( VCC ) + ( !VCC ))
-- \passeData|ClockTempo|Add0~102\ = CARRY(( \passeData|ClockTempo|Iteration\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(0),
	cin => GND,
	sumout => \passeData|ClockTempo|Add0~101_sumout\,
	cout => \passeData|ClockTempo|Add0~102\);

-- Location: LABCELL_X81_Y10_N48
\passeData|contaIteracoes|Iteration[0]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|contaIteracoes|Iteration[0]~2_combout\ = ( !\passeData|contaIteracoes|Iteration\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	combout => \passeData|contaIteracoes|Iteration[0]~2_combout\);

-- Location: FF_X82_Y10_N11
\passeData|contaIteracoes|Iteration[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \passeData|ChecaSeLimiteTempo|Equal0~combout\,
	asdata => \passeData|contaIteracoes|Iteration[0]~2_combout\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|contaIteracoes|Iteration\(0));

-- Location: LABCELL_X81_Y10_N12
\control|EA.E1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \control|EA.E1~0_combout\ = !\control|EA.E0~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \control|ALT_INV_EA.E0~q\,
	combout => \control|EA.E1~0_combout\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: LABCELL_X81_Y10_N21
\L1|btn0state.EsperaApertar~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \L1|btn0state.EsperaApertar~0_combout\ = !\KEY[0]~input_o\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[0]~input_o\,
	combout => \L1|btn0state.EsperaApertar~0_combout\);

-- Location: FF_X81_Y10_N22
\L1|btn0state.EsperaApertar\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \L1|btn0state.EsperaApertar~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L1|btn0state.EsperaApertar~q\);

-- Location: LABCELL_X81_Y10_N18
\L1|btn0next.SaidaAtiva~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \L1|btn0next.SaidaAtiva~0_combout\ = ( !\L1|btn0state.EsperaApertar~q\ & ( !\KEY[0]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[0]~input_o\,
	dataf => \L1|ALT_INV_btn0state.EsperaApertar~q\,
	combout => \L1|btn0next.SaidaAtiva~0_combout\);

-- Location: FF_X81_Y10_N20
\L1|btn0state.SaidaAtiva\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \L1|btn0next.SaidaAtiva~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L1|btn0state.SaidaAtiva~q\);

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: LABCELL_X81_Y10_N57
\L1|btn1state.EsperaApertar~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \L1|btn1state.EsperaApertar~0_combout\ = ( !\KEY[1]~input_o\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_KEY[1]~input_o\,
	combout => \L1|btn1state.EsperaApertar~0_combout\);

-- Location: FF_X81_Y10_N58
\L1|btn1state.EsperaApertar\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \L1|btn1state.EsperaApertar~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L1|btn1state.EsperaApertar~q\);

-- Location: LABCELL_X81_Y10_N24
\L1|btn1next.SaidaAtiva~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \L1|btn1next.SaidaAtiva~0_combout\ = ( !\L1|btn1state.EsperaApertar~q\ & ( !\KEY[1]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_KEY[1]~input_o\,
	dataf => \L1|ALT_INV_btn1state.EsperaApertar~q\,
	combout => \L1|btn1next.SaidaAtiva~0_combout\);

-- Location: FF_X81_Y10_N26
\L1|btn1state.SaidaAtiva\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \L1|btn1next.SaidaAtiva~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L1|btn1state.SaidaAtiva~q\);

-- Location: FF_X81_Y10_N14
\control|EA.E1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \control|EA.E1~0_combout\,
	clrn => \L1|ALT_INV_btn0state.SaidaAtiva~q\,
	ena => \L1|btn1state.SaidaAtiva~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control|EA.E1~q\);

-- Location: MLABCELL_X84_Y10_N27
\passeData|a3|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|a3|Mux2~0_combout\ = ( \passeData|contaIteracoes|Iteration\(0) & ( !\passeData|contaIteracoes|Iteration\(1) ) ) # ( !\passeData|contaIteracoes|Iteration\(0) & ( \passeData|contaIteracoes|Iteration\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	combout => \passeData|a3|Mux2~0_combout\);

-- Location: FF_X84_Y10_N17
\passeData|contaIteracoes|Iteration[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \passeData|ChecaSeLimiteTempo|Equal0~combout\,
	asdata => \passeData|a3|Mux2~0_combout\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|contaIteracoes|Iteration\(1));

-- Location: LABCELL_X83_Y10_N51
\passeData|contaIteracoes|Iteration[3]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|contaIteracoes|Iteration[3]~0_combout\ = ( \passeData|contaIteracoes|Iteration\(1) & ( !\passeData|contaIteracoes|Iteration\(3) $ (((!\control|EA.E2~q\) # ((!\passeData|contaIteracoes|Iteration\(0)) # 
-- (!\passeData|contaIteracoes|Iteration\(2))))) ) ) # ( !\passeData|contaIteracoes|Iteration\(1) & ( \passeData|contaIteracoes|Iteration\(3) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000111100000111100011110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \control|ALT_INV_EA.E2~q\,
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	combout => \passeData|contaIteracoes|Iteration[3]~0_combout\);

-- Location: FF_X83_Y10_N23
\passeData|contaIteracoes|Iteration[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \passeData|ChecaSeLimiteTempo|Equal0~combout\,
	asdata => \passeData|contaIteracoes|Iteration[3]~0_combout\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|contaIteracoes|Iteration\(3));

-- Location: LABCELL_X83_Y10_N54
\passeData|compDez|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|compDez|Equal0~0_combout\ = ( !\passeData|contaIteracoes|Iteration\(2) & ( (\passeData|contaIteracoes|Iteration\(3) & !\passeData|contaIteracoes|Iteration\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	combout => \passeData|compDez|Equal0~0_combout\);

-- Location: LABCELL_X83_Y10_N48
\control|Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \control|Selector2~0_combout\ = ( \passeData|compDez|Equal0~0_combout\ & ( (!\control|EA.E2~q\ & (((\control|EA.E1~q\ & \L1|btn1state.SaidaAtiva~q\)))) # (\control|EA.E2~q\ & ((!\passeData|contaIteracoes|Iteration\(0)) # ((\control|EA.E1~q\ & 
-- \L1|btn1state.SaidaAtiva~q\)))) ) ) # ( !\passeData|compDez|Equal0~0_combout\ & ( ((\control|EA.E1~q\ & \L1|btn1state.SaidaAtiva~q\)) # (\control|EA.E2~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101011111010101010101111101000100010011110100010001001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \control|ALT_INV_EA.E2~q\,
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \control|ALT_INV_EA.E1~q\,
	datad => \L1|ALT_INV_btn1state.SaidaAtiva~q\,
	dataf => \passeData|compDez|ALT_INV_Equal0~0_combout\,
	combout => \control|Selector2~0_combout\);

-- Location: FF_X83_Y10_N50
\control|EA.E2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \control|Selector2~0_combout\,
	clrn => \L1|ALT_INV_btn0state.SaidaAtiva~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control|EA.E2~q\);

-- Location: LABCELL_X83_Y10_N57
\control|Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \control|Selector3~0_combout\ = ( \passeData|contaIteracoes|Iteration\(0) & ( (!\control|EA.E2~q\ & (!\L1|btn1state.SaidaAtiva~q\ & ((\control|EA.E3~q\)))) # (\control|EA.E2~q\ & (((!\L1|btn1state.SaidaAtiva~q\ & \control|EA.E3~q\)) # 
-- (\passeData|compDez|Equal0~0_combout\))) ) ) # ( !\passeData|contaIteracoes|Iteration\(0) & ( (!\L1|btn1state.SaidaAtiva~q\ & \control|EA.E3~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000000001100110000000101110011010000010111001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \control|ALT_INV_EA.E2~q\,
	datab => \L1|ALT_INV_btn1state.SaidaAtiva~q\,
	datac => \passeData|compDez|ALT_INV_Equal0~0_combout\,
	datad => \control|ALT_INV_EA.E3~q\,
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	combout => \control|Selector3~0_combout\);

-- Location: FF_X83_Y10_N59
\control|EA.E3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \control|Selector3~0_combout\,
	clrn => \L1|ALT_INV_btn0state.SaidaAtiva~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control|EA.E3~q\);

-- Location: MLABCELL_X84_Y10_N6
\control|EA.E0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \control|EA.E0~0_combout\ = ( !\control|EA.E3~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \control|ALT_INV_EA.E3~q\,
	combout => \control|EA.E0~0_combout\);

-- Location: FF_X83_Y10_N17
\control|EA.E0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \control|EA.E0~0_combout\,
	clrn => \L1|ALT_INV_btn0state.SaidaAtiva~q\,
	sload => VCC,
	ena => \L1|btn1state.SaidaAtiva~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control|EA.E0~q\);

-- Location: IOIBUF_X4_Y0_N18
\SW[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: FF_X84_Y12_N47
\passeData|registradorNivel|B[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \SW[8]~input_o\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|registradorNivel|B\(0));

-- Location: IOIBUF_X2_Y0_N58
\SW[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: FF_X84_Y12_N14
\passeData|registradorNivel|B[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \SW[9]~input_o\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|registradorNivel|B\(1));

-- Location: LABCELL_X85_Y12_N45
\passeData|ClockTempo|LessThan0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~9_combout\ = ( \passeData|registradorNivel|B\(1) & ( ((\passeData|registradorNivel|B\(0) & \passeData|ClockTempo|Iteration\(27))) # (\passeData|ClockTempo|Iteration\(28)) ) ) # ( !\passeData|registradorNivel|B\(1) & ( 
-- (\passeData|ClockTempo|Iteration\(28) & \passeData|registradorNivel|B\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010001000100010001000101010101011101110101010101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(28),
	datab => \passeData|registradorNivel|ALT_INV_B\(0),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(27),
	dataf => \passeData|registradorNivel|ALT_INV_B\(1),
	combout => \passeData|ClockTempo|LessThan0~9_combout\);

-- Location: LABCELL_X83_Y12_N33
\passeData|ClockTempo|LessThan0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~8_combout\ = ( \passeData|ClockTempo|Iteration\(25) & ( \passeData|registradorNivel|B\(0) & ( \passeData|registradorNivel|B\(1) ) ) ) # ( \passeData|ClockTempo|Iteration\(25) & ( !\passeData|registradorNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|registradorNivel|ALT_INV_B\(1),
	datae => \passeData|ClockTempo|ALT_INV_Iteration\(25),
	dataf => \passeData|registradorNivel|ALT_INV_B\(0),
	combout => \passeData|ClockTempo|LessThan0~8_combout\);

-- Location: LABCELL_X85_Y12_N42
\passeData|ClockTempo|LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~0_combout\ = ( \passeData|ClockTempo|Iteration\(28) & ( (!\passeData|registradorNivel|B\(0) & (\passeData|ClockTempo|Iteration\(27) & !\passeData|registradorNivel|B\(1))) ) ) # ( !\passeData|ClockTempo|Iteration\(28) & ( 
-- (!\passeData|registradorNivel|B\(0) & (\passeData|ClockTempo|Iteration\(27) & \passeData|registradorNivel|B\(1))) # (\passeData|registradorNivel|B\(0) & (!\passeData|ClockTempo|Iteration\(27) $ (!\passeData|registradorNivel|B\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100111100000000110011110000001100000000000000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|registradorNivel|ALT_INV_B\(0),
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(27),
	datad => \passeData|registradorNivel|ALT_INV_B\(1),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(28),
	combout => \passeData|ClockTempo|LessThan0~0_combout\);

-- Location: MLABCELL_X84_Y12_N36
\passeData|resultadoUltimo|Equal2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|Equal2~1_combout\ = ( \passeData|registradorNivel|B\(1) & ( !\passeData|registradorNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \passeData|registradorNivel|ALT_INV_B\(0),
	dataf => \passeData|registradorNivel|ALT_INV_B\(1),
	combout => \passeData|resultadoUltimo|Equal2~1_combout\);

-- Location: MLABCELL_X84_Y12_N6
\passeData|resultadoUltimo|Equal2~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|Equal2~2_combout\ = ( !\passeData|registradorNivel|B\(1) & ( !\passeData|registradorNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \passeData|registradorNivel|ALT_INV_B\(0),
	dataf => \passeData|registradorNivel|ALT_INV_B\(1),
	combout => \passeData|resultadoUltimo|Equal2~2_combout\);

-- Location: MLABCELL_X84_Y12_N33
\passeData|tempo|Op[26]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|tempo|Op[26]~0_combout\ = ( \passeData|resultadoUltimo|Equal2~2_combout\ & ( (!\passeData|registradorNivel|B\(1)) # (!\passeData|resultadoUltimo|Equal2~1_combout\) ) ) # ( !\passeData|resultadoUltimo|Equal2~2_combout\ & ( 
-- (\passeData|registradorNivel|B\(1) & !\passeData|resultadoUltimo|Equal2~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000011111111111100001111111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|registradorNivel|ALT_INV_B\(1),
	datad => \passeData|resultadoUltimo|ALT_INV_Equal2~1_combout\,
	dataf => \passeData|resultadoUltimo|ALT_INV_Equal2~2_combout\,
	combout => \passeData|tempo|Op[26]~0_combout\);

-- Location: MLABCELL_X84_Y12_N51
\passeData|ClockTempo|LessThan0~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~10_combout\ = ( \passeData|ClockTempo|LessThan0~0_combout\ & ( \passeData|tempo|Op[26]~0_combout\ & ( (!\passeData|ClockTempo|LessThan0~9_combout\ & (\passeData|ChecaSeLimiteTempo|Equal0~7_combout\ & 
-- ((!\passeData|ClockTempo|LessThan0~8_combout\) # (!\passeData|ClockTempo|Iteration\(26))))) ) ) ) # ( !\passeData|ClockTempo|LessThan0~0_combout\ & ( \passeData|tempo|Op[26]~0_combout\ & ( (!\passeData|ClockTempo|LessThan0~9_combout\ & 
-- \passeData|ChecaSeLimiteTempo|Equal0~7_combout\) ) ) ) # ( \passeData|ClockTempo|LessThan0~0_combout\ & ( !\passeData|tempo|Op[26]~0_combout\ & ( (!\passeData|ClockTempo|LessThan0~9_combout\ & (!\passeData|ClockTempo|LessThan0~8_combout\ & 
-- (!\passeData|ClockTempo|Iteration\(26) & \passeData|ChecaSeLimiteTempo|Equal0~7_combout\))) ) ) ) # ( !\passeData|ClockTempo|LessThan0~0_combout\ & ( !\passeData|tempo|Op[26]~0_combout\ & ( (!\passeData|ClockTempo|LessThan0~9_combout\ & 
-- \passeData|ChecaSeLimiteTempo|Equal0~7_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001000000000000000101010100000000010101000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_LessThan0~9_combout\,
	datab => \passeData|ClockTempo|ALT_INV_LessThan0~8_combout\,
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(26),
	datad => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~7_combout\,
	datae => \passeData|ClockTempo|ALT_INV_LessThan0~0_combout\,
	dataf => \passeData|tempo|ALT_INV_Op[26]~0_combout\,
	combout => \passeData|ClockTempo|LessThan0~10_combout\);

-- Location: LABCELL_X85_Y12_N57
\passeData|ChecaSeLimiteTempo|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~0_combout\ = ( \passeData|registradorNivel|B\(1) & ( \passeData|ClockTempo|Iteration\(25) ) ) # ( !\passeData|registradorNivel|B\(1) & ( \passeData|ClockTempo|Iteration\(25) & ( !\passeData|registradorNivel|B\(0) ) ) ) 
-- # ( !\passeData|registradorNivel|B\(1) & ( !\passeData|ClockTempo|Iteration\(25) & ( \passeData|registradorNivel|B\(0) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000000000000011110000111100001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|registradorNivel|ALT_INV_B\(0),
	datae => \passeData|registradorNivel|ALT_INV_B\(1),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(25),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~0_combout\);

-- Location: MLABCELL_X84_Y12_N15
\passeData|ClockTempo|LessThan0~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~7_combout\ = ( !\passeData|ChecaSeLimiteTempo|Equal0~0_combout\ & ( \passeData|ClockTempo|Iteration\(24) & ( (\passeData|ClockTempo|LessThan0~0_combout\ & (\passeData|resultadoUltimo|Equal2~1_combout\ & 
-- (!\passeData|tempo|Op[26]~0_combout\ $ (\passeData|ClockTempo|Iteration\(26))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010010000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|tempo|ALT_INV_Op[26]~0_combout\,
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(26),
	datac => \passeData|ClockTempo|ALT_INV_LessThan0~0_combout\,
	datad => \passeData|resultadoUltimo|ALT_INV_Equal2~1_combout\,
	datae => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~0_combout\,
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(24),
	combout => \passeData|ClockTempo|LessThan0~7_combout\);

-- Location: MLABCELL_X84_Y12_N9
\passeData|ChecaSeLimiteTempo|Equal0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~5_combout\ = ( \passeData|ClockTempo|Iteration\(19) & ( (!\passeData|registradorNivel|B\(1) & (!\passeData|ClockTempo|Iteration\(20) & (!\passeData|ClockTempo|Iteration\(21) $ (\passeData|registradorNivel|B\(0))))) ) ) 
-- # ( !\passeData|ClockTempo|Iteration\(19) & ( (\passeData|ClockTempo|Iteration\(21) & (\passeData|registradorNivel|B\(1) & \passeData|ClockTempo|Iteration\(20))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010001000000000001000110000100000000001000010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(21),
	datab => \passeData|registradorNivel|ALT_INV_B\(1),
	datac => \passeData|registradorNivel|ALT_INV_B\(0),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(20),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(19),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~5_combout\);

-- Location: MLABCELL_X84_Y12_N3
\passeData|ChecaSeLimiteTempo|Equal0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~6_combout\ = ( \passeData|tempo|Op[26]~0_combout\ & ( (\passeData|ChecaSeLimiteTempo|Equal0~5_combout\ & \passeData|ClockTempo|Iteration\(18)) ) ) # ( !\passeData|tempo|Op[26]~0_combout\ & ( 
-- (\passeData|ChecaSeLimiteTempo|Equal0~5_combout\ & !\passeData|ClockTempo|Iteration\(18)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~5_combout\,
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(18),
	dataf => \passeData|tempo|ALT_INV_Op[26]~0_combout\,
	combout => \passeData|ChecaSeLimiteTempo|Equal0~6_combout\);

-- Location: LABCELL_X85_Y12_N48
\passeData|ChecaSeLimiteTempo|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~1_combout\ = ( \passeData|ClockTempo|Iteration\(24) & ( (\passeData|ClockTempo|Iteration\(22) & (\passeData|ClockTempo|Iteration\(23) & ((!\passeData|registradorNivel|B\(1)) # (\passeData|registradorNivel|B\(0))))) ) ) 
-- # ( !\passeData|ClockTempo|Iteration\(24) & ( (\passeData|registradorNivel|B\(1) & (\passeData|ClockTempo|Iteration\(22) & (!\passeData|registradorNivel|B\(0) & \passeData|ClockTempo|Iteration\(23)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000001000000000000001000110000000000100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|registradorNivel|ALT_INV_B\(1),
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(22),
	datac => \passeData|registradorNivel|ALT_INV_B\(0),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(23),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(24),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~1_combout\);

-- Location: LABCELL_X85_Y12_N36
\passeData|ChecaSeLimiteTempo|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~2_combout\ = ( \passeData|tempo|Op[26]~0_combout\ & ( \passeData|ChecaSeLimiteTempo|Equal0~1_combout\ & ( (!\passeData|ChecaSeLimiteTempo|Equal0~0_combout\ & (\passeData|ClockTempo|Iteration\(26) & 
-- \passeData|ClockTempo|LessThan0~0_combout\)) ) ) ) # ( !\passeData|tempo|Op[26]~0_combout\ & ( \passeData|ChecaSeLimiteTempo|Equal0~1_combout\ & ( (!\passeData|ChecaSeLimiteTempo|Equal0~0_combout\ & (!\passeData|ClockTempo|Iteration\(26) & 
-- \passeData|ClockTempo|LessThan0~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000110000000000000000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~0_combout\,
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(26),
	datad => \passeData|ClockTempo|ALT_INV_LessThan0~0_combout\,
	datae => \passeData|tempo|ALT_INV_Op[26]~0_combout\,
	dataf => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~1_combout\,
	combout => \passeData|ChecaSeLimiteTempo|Equal0~2_combout\);

-- Location: LABCELL_X83_Y12_N15
\passeData|ClockTempo|LessThan0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~5_combout\ = ( \passeData|ClockTempo|Iteration\(21) & ( \passeData|ClockTempo|Iteration\(19) & ( ((!\passeData|registradorNivel|B\(0) & !\passeData|registradorNivel|B\(1))) # (\passeData|ClockTempo|Iteration\(20)) ) ) ) # ( 
-- !\passeData|ClockTempo|Iteration\(21) & ( \passeData|ClockTempo|Iteration\(19) & ( (!\passeData|registradorNivel|B\(0) & (!\passeData|registradorNivel|B\(1) & \passeData|ClockTempo|Iteration\(20))) ) ) ) # ( \passeData|ClockTempo|Iteration\(21) & ( 
-- !\passeData|ClockTempo|Iteration\(19) & ( (!\passeData|registradorNivel|B\(1) & ((!\passeData|registradorNivel|B\(0)) # (\passeData|ClockTempo|Iteration\(20)))) ) ) ) # ( !\passeData|ClockTempo|Iteration\(21) & ( !\passeData|ClockTempo|Iteration\(19) & ( 
-- (!\passeData|registradorNivel|B\(0) & (!\passeData|registradorNivel|B\(1) & \passeData|ClockTempo|Iteration\(20))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000100011001000110000001000000010001000111110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|registradorNivel|ALT_INV_B\(0),
	datab => \passeData|registradorNivel|ALT_INV_B\(1),
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(20),
	datae => \passeData|ClockTempo|ALT_INV_Iteration\(21),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(19),
	combout => \passeData|ClockTempo|LessThan0~5_combout\);

-- Location: MLABCELL_X84_Y12_N21
\passeData|ClockTempo|LessThan0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~6_combout\ = ( !\passeData|ClockTempo|LessThan0~5_combout\ & ( ((!\passeData|ChecaSeLimiteTempo|Equal0~5_combout\) # (!\passeData|ClockTempo|Iteration\(18))) # (\passeData|tempo|Op[26]~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110101111111111111010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|tempo|ALT_INV_Op[26]~0_combout\,
	datac => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~5_combout\,
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(18),
	dataf => \passeData|ClockTempo|ALT_INV_LessThan0~5_combout\,
	combout => \passeData|ClockTempo|LessThan0~6_combout\);

-- Location: MLABCELL_X84_Y12_N54
\passeData|ChecaSeLimiteTempo|Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~3_combout\ = ( \passeData|ClockTempo|Iteration\(17) & ( \passeData|ClockTempo|Iteration\(16) & ( (\passeData|ClockTempo|Iteration\(14) & (\passeData|registradorNivel|B\(0) & (\passeData|ClockTempo|Iteration\(15) & 
-- !\passeData|registradorNivel|B\(1)))) ) ) ) # ( !\passeData|ClockTempo|Iteration\(17) & ( \passeData|ClockTempo|Iteration\(16) & ( (\passeData|ClockTempo|Iteration\(14) & ((!\passeData|registradorNivel|B\(0) & (!\passeData|ClockTempo|Iteration\(15) & 
-- !\passeData|registradorNivel|B\(1))) # (\passeData|registradorNivel|B\(0) & (\passeData|ClockTempo|Iteration\(15) & \passeData|registradorNivel|B\(1))))) ) ) ) # ( !\passeData|ClockTempo|Iteration\(17) & ( !\passeData|ClockTempo|Iteration\(16) & ( 
-- (\passeData|ClockTempo|Iteration\(14) & (!\passeData|registradorNivel|B\(0) & (\passeData|ClockTempo|Iteration\(15) & \passeData|registradorNivel|B\(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000100000000000000000001000000000000010000000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(14),
	datab => \passeData|registradorNivel|ALT_INV_B\(0),
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(15),
	datad => \passeData|registradorNivel|ALT_INV_B\(1),
	datae => \passeData|ClockTempo|ALT_INV_Iteration\(17),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(16),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~3_combout\);

-- Location: MLABCELL_X84_Y12_N0
\passeData|ClockTempo|LessThan0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~3_combout\ = ( \passeData|registradorNivel|B\(1) & ( ((\passeData|ClockTempo|Iteration\(16) & !\passeData|registradorNivel|B\(0))) # (\passeData|ClockTempo|Iteration\(17)) ) ) # ( !\passeData|registradorNivel|B\(1) & ( 
-- (!\passeData|registradorNivel|B\(0) & (((\passeData|ClockTempo|Iteration\(16) & \passeData|ClockTempo|Iteration\(15))) # (\passeData|ClockTempo|Iteration\(17)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010011001100000001001100110001000100111111110100010011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(16),
	datab => \passeData|registradorNivel|ALT_INV_B\(0),
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(15),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(17),
	dataf => \passeData|registradorNivel|ALT_INV_B\(1),
	combout => \passeData|ClockTempo|LessThan0~3_combout\);

-- Location: MLABCELL_X84_Y12_N27
\passeData|ClockTempo|LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~1_combout\ = ( \passeData|ClockTempo|Iteration\(7) & ( (!\passeData|ClockTempo|Iteration\(9) & ((!\passeData|ClockTempo|Iteration\(8)) # ((!\passeData|registradorNivel|B\(1) & \passeData|registradorNivel|B\(0))))) ) ) # ( 
-- !\passeData|ClockTempo|Iteration\(7) & ( (!\passeData|ClockTempo|Iteration\(9) & ((!\passeData|ClockTempo|Iteration\(8)) # (!\passeData|registradorNivel|B\(1) $ (!\passeData|registradorNivel|B\(0))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011111000000000101111100000000010101110000000001010111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(8),
	datab => \passeData|registradorNivel|ALT_INV_B\(1),
	datac => \passeData|registradorNivel|ALT_INV_B\(0),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(9),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(7),
	combout => \passeData|ClockTempo|LessThan0~1_combout\);

-- Location: MLABCELL_X84_Y12_N42
\passeData|ClockTempo|LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~2_combout\ = ( \passeData|ClockTempo|LessThan0~1_combout\ & ( \passeData|ClockTempo|Iteration\(11) & ( (!\passeData|ClockTempo|Iteration\(12) & (\passeData|registradorNivel|B\(1) & !\passeData|registradorNivel|B\(0))) ) ) ) 
-- # ( !\passeData|ClockTempo|LessThan0~1_combout\ & ( \passeData|ClockTempo|Iteration\(11) & ( (!\passeData|ClockTempo|Iteration\(12) & (\passeData|registradorNivel|B\(1) & !\passeData|registradorNivel|B\(0))) ) ) ) # ( 
-- \passeData|ClockTempo|LessThan0~1_combout\ & ( !\passeData|ClockTempo|Iteration\(11) & ( (!\passeData|ClockTempo|Iteration\(10) & ((!\passeData|ClockTempo|Iteration\(12)) # ((\passeData|registradorNivel|B\(1) & !\passeData|registradorNivel|B\(0))))) # 
-- (\passeData|ClockTempo|Iteration\(10) & (!\passeData|ClockTempo|Iteration\(12) & ((!\passeData|registradorNivel|B\(0))))) ) ) ) # ( !\passeData|ClockTempo|LessThan0~1_combout\ & ( !\passeData|ClockTempo|Iteration\(11) & ( 
-- (!\passeData|ClockTempo|Iteration\(12) & (!\passeData|registradorNivel|B\(0) & ((!\passeData|ClockTempo|Iteration\(10)) # (\passeData|registradorNivel|B\(1))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110000000000110011101000100000001100000000000000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(10),
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(12),
	datac => \passeData|registradorNivel|ALT_INV_B\(1),
	datad => \passeData|registradorNivel|ALT_INV_B\(0),
	datae => \passeData|ClockTempo|ALT_INV_LessThan0~1_combout\,
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(11),
	combout => \passeData|ClockTempo|LessThan0~2_combout\);

-- Location: MLABCELL_X84_Y12_N18
\passeData|ClockTempo|LessThan0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~4_combout\ = ( \passeData|ClockTempo|LessThan0~2_combout\ & ( (!\passeData|ClockTempo|LessThan0~3_combout\ & (((!\passeData|ClockTempo|Iteration\(13)) # (!\passeData|ChecaSeLimiteTempo|Equal0~3_combout\)) # 
-- (\passeData|tempo|Op[26]~0_combout\))) ) ) # ( !\passeData|ClockTempo|LessThan0~2_combout\ & ( (!\passeData|ClockTempo|LessThan0~3_combout\ & ((!\passeData|ChecaSeLimiteTempo|Equal0~3_combout\) # ((\passeData|tempo|Op[26]~0_combout\ & 
-- !\passeData|ClockTempo|Iteration\(13))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010000000000111101000000000011111101000000001111110100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|tempo|ALT_INV_Op[26]~0_combout\,
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(13),
	datac => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~3_combout\,
	datad => \passeData|ClockTempo|ALT_INV_LessThan0~3_combout\,
	dataf => \passeData|ClockTempo|ALT_INV_LessThan0~2_combout\,
	combout => \passeData|ClockTempo|LessThan0~4_combout\);

-- Location: MLABCELL_X84_Y13_N30
\passeData|ClockTempo|LessThan0~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|LessThan0~11_combout\ = ( \passeData|ClockTempo|LessThan0~6_combout\ & ( \passeData|ClockTempo|LessThan0~4_combout\ & ( (!\passeData|ClockTempo|LessThan0~10_combout\) # (\passeData|ClockTempo|LessThan0~7_combout\) ) ) ) # ( 
-- !\passeData|ClockTempo|LessThan0~6_combout\ & ( \passeData|ClockTempo|LessThan0~4_combout\ & ( (!\passeData|ClockTempo|LessThan0~10_combout\) # ((\passeData|ChecaSeLimiteTempo|Equal0~2_combout\) # (\passeData|ClockTempo|LessThan0~7_combout\)) ) ) ) # ( 
-- \passeData|ClockTempo|LessThan0~6_combout\ & ( !\passeData|ClockTempo|LessThan0~4_combout\ & ( (!\passeData|ClockTempo|LessThan0~10_combout\) # (((\passeData|ChecaSeLimiteTempo|Equal0~6_combout\ & \passeData|ChecaSeLimiteTempo|Equal0~2_combout\)) # 
-- (\passeData|ClockTempo|LessThan0~7_combout\)) ) ) ) # ( !\passeData|ClockTempo|LessThan0~6_combout\ & ( !\passeData|ClockTempo|LessThan0~4_combout\ & ( (!\passeData|ClockTempo|LessThan0~10_combout\) # ((\passeData|ChecaSeLimiteTempo|Equal0~2_combout\) # 
-- (\passeData|ClockTempo|LessThan0~7_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011101111111111101110111011111110111011111111111011101110111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_LessThan0~10_combout\,
	datab => \passeData|ClockTempo|ALT_INV_LessThan0~7_combout\,
	datac => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~6_combout\,
	datad => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~2_combout\,
	datae => \passeData|ClockTempo|ALT_INV_LessThan0~6_combout\,
	dataf => \passeData|ClockTempo|ALT_INV_LessThan0~4_combout\,
	combout => \passeData|ClockTempo|LessThan0~11_combout\);

-- Location: FF_X84_Y13_N29
\passeData|ClockTempo|Iteration[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|ClockTempo|Add0~101_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(0));

-- Location: LABCELL_X85_Y13_N3
\passeData|ClockTempo|Add0~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~97_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(1) ) + ( GND ) + ( \passeData|ClockTempo|Add0~102\ ))
-- \passeData|ClockTempo|Add0~98\ = CARRY(( \passeData|ClockTempo|Iteration\(1) ) + ( GND ) + ( \passeData|ClockTempo|Add0~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(1),
	cin => \passeData|ClockTempo|Add0~102\,
	sumout => \passeData|ClockTempo|Add0~97_sumout\,
	cout => \passeData|ClockTempo|Add0~98\);

-- Location: FF_X84_Y13_N41
\passeData|ClockTempo|Iteration[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|ClockTempo|Add0~97_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(1));

-- Location: LABCELL_X85_Y13_N6
\passeData|ClockTempo|Add0~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~89_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(2) ) + ( GND ) + ( \passeData|ClockTempo|Add0~98\ ))
-- \passeData|ClockTempo|Add0~90\ = CARRY(( \passeData|ClockTempo|Iteration\(2) ) + ( GND ) + ( \passeData|ClockTempo|Add0~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(2),
	cin => \passeData|ClockTempo|Add0~98\,
	sumout => \passeData|ClockTempo|Add0~89_sumout\,
	cout => \passeData|ClockTempo|Add0~90\);

-- Location: FF_X84_Y13_N5
\passeData|ClockTempo|Iteration[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|ClockTempo|Add0~89_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(2));

-- Location: LABCELL_X85_Y13_N9
\passeData|ClockTempo|Add0~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~81_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(3) ) + ( GND ) + ( \passeData|ClockTempo|Add0~90\ ))
-- \passeData|ClockTempo|Add0~82\ = CARRY(( \passeData|ClockTempo|Iteration\(3) ) + ( GND ) + ( \passeData|ClockTempo|Add0~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(3),
	cin => \passeData|ClockTempo|Add0~90\,
	sumout => \passeData|ClockTempo|Add0~81_sumout\,
	cout => \passeData|ClockTempo|Add0~82\);

-- Location: MLABCELL_X84_Y13_N51
\passeData|ClockTempo|Iteration[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Iteration[3]~feeder_combout\ = ( \passeData|ClockTempo|Add0~81_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \passeData|ClockTempo|ALT_INV_Add0~81_sumout\,
	combout => \passeData|ClockTempo|Iteration[3]~feeder_combout\);

-- Location: FF_X84_Y13_N53
\passeData|ClockTempo|Iteration[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Iteration[3]~feeder_combout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(3));

-- Location: LABCELL_X85_Y13_N12
\passeData|ClockTempo|Add0~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~109_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(4) ) + ( GND ) + ( \passeData|ClockTempo|Add0~82\ ))
-- \passeData|ClockTempo|Add0~110\ = CARRY(( \passeData|ClockTempo|Iteration\(4) ) + ( GND ) + ( \passeData|ClockTempo|Add0~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(4),
	cin => \passeData|ClockTempo|Add0~82\,
	sumout => \passeData|ClockTempo|Add0~109_sumout\,
	cout => \passeData|ClockTempo|Add0~110\);

-- Location: FF_X84_Y13_N17
\passeData|ClockTempo|Iteration[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|ClockTempo|Add0~109_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(4));

-- Location: LABCELL_X85_Y13_N15
\passeData|ClockTempo|Add0~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~113_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(5) ) + ( GND ) + ( \passeData|ClockTempo|Add0~110\ ))
-- \passeData|ClockTempo|Add0~114\ = CARRY(( \passeData|ClockTempo|Iteration\(5) ) + ( GND ) + ( \passeData|ClockTempo|Add0~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(5),
	cin => \passeData|ClockTempo|Add0~110\,
	sumout => \passeData|ClockTempo|Add0~113_sumout\,
	cout => \passeData|ClockTempo|Add0~114\);

-- Location: FF_X84_Y13_N47
\passeData|ClockTempo|Iteration[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|ClockTempo|Add0~113_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(5));

-- Location: LABCELL_X85_Y13_N18
\passeData|ClockTempo|Add0~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~105_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(6) ) + ( GND ) + ( \passeData|ClockTempo|Add0~114\ ))
-- \passeData|ClockTempo|Add0~106\ = CARRY(( \passeData|ClockTempo|Iteration\(6) ) + ( GND ) + ( \passeData|ClockTempo|Add0~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(6),
	cin => \passeData|ClockTempo|Add0~114\,
	sumout => \passeData|ClockTempo|Add0~105_sumout\,
	cout => \passeData|ClockTempo|Add0~106\);

-- Location: FF_X84_Y13_N35
\passeData|ClockTempo|Iteration[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|ClockTempo|Add0~105_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(6));

-- Location: LABCELL_X85_Y13_N21
\passeData|ClockTempo|Add0~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~77_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(7) ) + ( GND ) + ( \passeData|ClockTempo|Add0~106\ ))
-- \passeData|ClockTempo|Add0~78\ = CARRY(( \passeData|ClockTempo|Iteration\(7) ) + ( GND ) + ( \passeData|ClockTempo|Add0~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(7),
	cin => \passeData|ClockTempo|Add0~106\,
	sumout => \passeData|ClockTempo|Add0~77_sumout\,
	cout => \passeData|ClockTempo|Add0~78\);

-- Location: FF_X84_Y13_N23
\passeData|ClockTempo|Iteration[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|ClockTempo|Add0~77_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	sload => VCC,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(7));

-- Location: LABCELL_X85_Y13_N24
\passeData|ClockTempo|Add0~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~125_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(8) ) + ( GND ) + ( \passeData|ClockTempo|Add0~78\ ))
-- \passeData|ClockTempo|Add0~126\ = CARRY(( \passeData|ClockTempo|Iteration\(8) ) + ( GND ) + ( \passeData|ClockTempo|Add0~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(8),
	cin => \passeData|ClockTempo|Add0~78\,
	sumout => \passeData|ClockTempo|Add0~125_sumout\,
	cout => \passeData|ClockTempo|Add0~126\);

-- Location: FF_X85_Y13_N26
\passeData|ClockTempo|Iteration[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~125_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(8));

-- Location: LABCELL_X85_Y13_N27
\passeData|ClockTempo|Add0~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~93_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(9) ) + ( GND ) + ( \passeData|ClockTempo|Add0~126\ ))
-- \passeData|ClockTempo|Add0~94\ = CARRY(( \passeData|ClockTempo|Iteration\(9) ) + ( GND ) + ( \passeData|ClockTempo|Add0~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(9),
	cin => \passeData|ClockTempo|Add0~126\,
	sumout => \passeData|ClockTempo|Add0~93_sumout\,
	cout => \passeData|ClockTempo|Add0~94\);

-- Location: FF_X85_Y13_N29
\passeData|ClockTempo|Iteration[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~93_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(9));

-- Location: LABCELL_X85_Y13_N30
\passeData|ClockTempo|Add0~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~117_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(10) ) + ( GND ) + ( \passeData|ClockTempo|Add0~94\ ))
-- \passeData|ClockTempo|Add0~118\ = CARRY(( \passeData|ClockTempo|Iteration\(10) ) + ( GND ) + ( \passeData|ClockTempo|Add0~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(10),
	cin => \passeData|ClockTempo|Add0~94\,
	sumout => \passeData|ClockTempo|Add0~117_sumout\,
	cout => \passeData|ClockTempo|Add0~118\);

-- Location: FF_X85_Y13_N32
\passeData|ClockTempo|Iteration[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~117_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(10));

-- Location: LABCELL_X85_Y13_N33
\passeData|ClockTempo|Add0~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~85_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(11) ) + ( GND ) + ( \passeData|ClockTempo|Add0~118\ ))
-- \passeData|ClockTempo|Add0~86\ = CARRY(( \passeData|ClockTempo|Iteration\(11) ) + ( GND ) + ( \passeData|ClockTempo|Add0~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(11),
	cin => \passeData|ClockTempo|Add0~118\,
	sumout => \passeData|ClockTempo|Add0~85_sumout\,
	cout => \passeData|ClockTempo|Add0~86\);

-- Location: FF_X85_Y13_N35
\passeData|ClockTempo|Iteration[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~85_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(11));

-- Location: LABCELL_X85_Y13_N36
\passeData|ClockTempo|Add0~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~121_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(12) ) + ( GND ) + ( \passeData|ClockTempo|Add0~86\ ))
-- \passeData|ClockTempo|Add0~122\ = CARRY(( \passeData|ClockTempo|Iteration\(12) ) + ( GND ) + ( \passeData|ClockTempo|Add0~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(12),
	cin => \passeData|ClockTempo|Add0~86\,
	sumout => \passeData|ClockTempo|Add0~121_sumout\,
	cout => \passeData|ClockTempo|Add0~122\);

-- Location: FF_X85_Y13_N38
\passeData|ClockTempo|Iteration[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~121_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(12));

-- Location: LABCELL_X85_Y13_N39
\passeData|ClockTempo|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~29_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(13) ) + ( GND ) + ( \passeData|ClockTempo|Add0~122\ ))
-- \passeData|ClockTempo|Add0~30\ = CARRY(( \passeData|ClockTempo|Iteration\(13) ) + ( GND ) + ( \passeData|ClockTempo|Add0~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(13),
	cin => \passeData|ClockTempo|Add0~122\,
	sumout => \passeData|ClockTempo|Add0~29_sumout\,
	cout => \passeData|ClockTempo|Add0~30\);

-- Location: FF_X85_Y13_N41
\passeData|ClockTempo|Iteration[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~29_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(13));

-- Location: LABCELL_X85_Y13_N42
\passeData|ClockTempo|Add0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~41_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(14) ) + ( GND ) + ( \passeData|ClockTempo|Add0~30\ ))
-- \passeData|ClockTempo|Add0~42\ = CARRY(( \passeData|ClockTempo|Iteration\(14) ) + ( GND ) + ( \passeData|ClockTempo|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(14),
	cin => \passeData|ClockTempo|Add0~30\,
	sumout => \passeData|ClockTempo|Add0~41_sumout\,
	cout => \passeData|ClockTempo|Add0~42\);

-- Location: FF_X85_Y13_N44
\passeData|ClockTempo|Iteration[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~41_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(14));

-- Location: LABCELL_X85_Y13_N45
\passeData|ClockTempo|Add0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~45_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(15) ) + ( GND ) + ( \passeData|ClockTempo|Add0~42\ ))
-- \passeData|ClockTempo|Add0~46\ = CARRY(( \passeData|ClockTempo|Iteration\(15) ) + ( GND ) + ( \passeData|ClockTempo|Add0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(15),
	cin => \passeData|ClockTempo|Add0~42\,
	sumout => \passeData|ClockTempo|Add0~45_sumout\,
	cout => \passeData|ClockTempo|Add0~46\);

-- Location: FF_X85_Y13_N47
\passeData|ClockTempo|Iteration[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~45_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(15));

-- Location: LABCELL_X85_Y13_N48
\passeData|ClockTempo|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~33_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(16) ) + ( GND ) + ( \passeData|ClockTempo|Add0~46\ ))
-- \passeData|ClockTempo|Add0~34\ = CARRY(( \passeData|ClockTempo|Iteration\(16) ) + ( GND ) + ( \passeData|ClockTempo|Add0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(16),
	cin => \passeData|ClockTempo|Add0~46\,
	sumout => \passeData|ClockTempo|Add0~33_sumout\,
	cout => \passeData|ClockTempo|Add0~34\);

-- Location: FF_X85_Y13_N50
\passeData|ClockTempo|Iteration[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~33_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(16));

-- Location: LABCELL_X85_Y13_N51
\passeData|ClockTempo|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~37_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(17) ) + ( GND ) + ( \passeData|ClockTempo|Add0~34\ ))
-- \passeData|ClockTempo|Add0~38\ = CARRY(( \passeData|ClockTempo|Iteration\(17) ) + ( GND ) + ( \passeData|ClockTempo|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(17),
	cin => \passeData|ClockTempo|Add0~34\,
	sumout => \passeData|ClockTempo|Add0~37_sumout\,
	cout => \passeData|ClockTempo|Add0~38\);

-- Location: FF_X85_Y13_N53
\passeData|ClockTempo|Iteration[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~37_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(17));

-- Location: LABCELL_X85_Y13_N54
\passeData|ClockTempo|Add0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~49_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(18) ) + ( GND ) + ( \passeData|ClockTempo|Add0~38\ ))
-- \passeData|ClockTempo|Add0~50\ = CARRY(( \passeData|ClockTempo|Iteration\(18) ) + ( GND ) + ( \passeData|ClockTempo|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(18),
	cin => \passeData|ClockTempo|Add0~38\,
	sumout => \passeData|ClockTempo|Add0~49_sumout\,
	cout => \passeData|ClockTempo|Add0~50\);

-- Location: FF_X85_Y13_N56
\passeData|ClockTempo|Iteration[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~49_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(18));

-- Location: LABCELL_X85_Y13_N57
\passeData|ClockTempo|Add0~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~57_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(19) ) + ( GND ) + ( \passeData|ClockTempo|Add0~50\ ))
-- \passeData|ClockTempo|Add0~58\ = CARRY(( \passeData|ClockTempo|Iteration\(19) ) + ( GND ) + ( \passeData|ClockTempo|Add0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(19),
	cin => \passeData|ClockTempo|Add0~50\,
	sumout => \passeData|ClockTempo|Add0~57_sumout\,
	cout => \passeData|ClockTempo|Add0~58\);

-- Location: FF_X85_Y13_N59
\passeData|ClockTempo|Iteration[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~57_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(19));

-- Location: LABCELL_X85_Y12_N0
\passeData|ClockTempo|Add0~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~61_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(20) ) + ( GND ) + ( \passeData|ClockTempo|Add0~58\ ))
-- \passeData|ClockTempo|Add0~62\ = CARRY(( \passeData|ClockTempo|Iteration\(20) ) + ( GND ) + ( \passeData|ClockTempo|Add0~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(20),
	cin => \passeData|ClockTempo|Add0~58\,
	sumout => \passeData|ClockTempo|Add0~61_sumout\,
	cout => \passeData|ClockTempo|Add0~62\);

-- Location: FF_X85_Y12_N2
\passeData|ClockTempo|Iteration[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~61_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(20));

-- Location: LABCELL_X85_Y12_N3
\passeData|ClockTempo|Add0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~53_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(21) ) + ( GND ) + ( \passeData|ClockTempo|Add0~62\ ))
-- \passeData|ClockTempo|Add0~54\ = CARRY(( \passeData|ClockTempo|Iteration\(21) ) + ( GND ) + ( \passeData|ClockTempo|Add0~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(21),
	cin => \passeData|ClockTempo|Add0~62\,
	sumout => \passeData|ClockTempo|Add0~53_sumout\,
	cout => \passeData|ClockTempo|Add0~54\);

-- Location: FF_X85_Y12_N5
\passeData|ClockTempo|Iteration[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~53_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(21));

-- Location: LABCELL_X85_Y12_N6
\passeData|ClockTempo|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~25_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(22) ) + ( GND ) + ( \passeData|ClockTempo|Add0~54\ ))
-- \passeData|ClockTempo|Add0~26\ = CARRY(( \passeData|ClockTempo|Iteration\(22) ) + ( GND ) + ( \passeData|ClockTempo|Add0~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(22),
	cin => \passeData|ClockTempo|Add0~54\,
	sumout => \passeData|ClockTempo|Add0~25_sumout\,
	cout => \passeData|ClockTempo|Add0~26\);

-- Location: FF_X85_Y12_N8
\passeData|ClockTempo|Iteration[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~25_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(22));

-- Location: LABCELL_X85_Y12_N9
\passeData|ClockTempo|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~21_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(23) ) + ( GND ) + ( \passeData|ClockTempo|Add0~26\ ))
-- \passeData|ClockTempo|Add0~22\ = CARRY(( \passeData|ClockTempo|Iteration\(23) ) + ( GND ) + ( \passeData|ClockTempo|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(23),
	cin => \passeData|ClockTempo|Add0~26\,
	sumout => \passeData|ClockTempo|Add0~21_sumout\,
	cout => \passeData|ClockTempo|Add0~22\);

-- Location: FF_X85_Y12_N11
\passeData|ClockTempo|Iteration[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~21_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(23));

-- Location: LABCELL_X85_Y12_N12
\passeData|ClockTempo|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~17_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(24) ) + ( GND ) + ( \passeData|ClockTempo|Add0~22\ ))
-- \passeData|ClockTempo|Add0~18\ = CARRY(( \passeData|ClockTempo|Iteration\(24) ) + ( GND ) + ( \passeData|ClockTempo|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(24),
	cin => \passeData|ClockTempo|Add0~22\,
	sumout => \passeData|ClockTempo|Add0~17_sumout\,
	cout => \passeData|ClockTempo|Add0~18\);

-- Location: FF_X85_Y12_N14
\passeData|ClockTempo|Iteration[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~17_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(24));

-- Location: LABCELL_X85_Y12_N15
\passeData|ClockTempo|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~1_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(25) ) + ( GND ) + ( \passeData|ClockTempo|Add0~18\ ))
-- \passeData|ClockTempo|Add0~2\ = CARRY(( \passeData|ClockTempo|Iteration\(25) ) + ( GND ) + ( \passeData|ClockTempo|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(25),
	cin => \passeData|ClockTempo|Add0~18\,
	sumout => \passeData|ClockTempo|Add0~1_sumout\,
	cout => \passeData|ClockTempo|Add0~2\);

-- Location: FF_X85_Y12_N17
\passeData|ClockTempo|Iteration[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~1_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(25));

-- Location: LABCELL_X85_Y12_N18
\passeData|ClockTempo|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~5_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(26) ) + ( GND ) + ( \passeData|ClockTempo|Add0~2\ ))
-- \passeData|ClockTempo|Add0~6\ = CARRY(( \passeData|ClockTempo|Iteration\(26) ) + ( GND ) + ( \passeData|ClockTempo|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(26),
	cin => \passeData|ClockTempo|Add0~2\,
	sumout => \passeData|ClockTempo|Add0~5_sumout\,
	cout => \passeData|ClockTempo|Add0~6\);

-- Location: FF_X85_Y12_N20
\passeData|ClockTempo|Iteration[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~5_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(26));

-- Location: LABCELL_X85_Y12_N21
\passeData|ClockTempo|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~9_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(27) ) + ( GND ) + ( \passeData|ClockTempo|Add0~6\ ))
-- \passeData|ClockTempo|Add0~10\ = CARRY(( \passeData|ClockTempo|Iteration\(27) ) + ( GND ) + ( \passeData|ClockTempo|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(27),
	cin => \passeData|ClockTempo|Add0~6\,
	sumout => \passeData|ClockTempo|Add0~9_sumout\,
	cout => \passeData|ClockTempo|Add0~10\);

-- Location: FF_X85_Y12_N23
\passeData|ClockTempo|Iteration[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~9_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(27));

-- Location: LABCELL_X85_Y12_N24
\passeData|ClockTempo|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~13_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(28) ) + ( GND ) + ( \passeData|ClockTempo|Add0~10\ ))
-- \passeData|ClockTempo|Add0~14\ = CARRY(( \passeData|ClockTempo|Iteration\(28) ) + ( GND ) + ( \passeData|ClockTempo|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(28),
	cin => \passeData|ClockTempo|Add0~10\,
	sumout => \passeData|ClockTempo|Add0~13_sumout\,
	cout => \passeData|ClockTempo|Add0~14\);

-- Location: FF_X85_Y12_N26
\passeData|ClockTempo|Iteration[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~13_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(28));

-- Location: LABCELL_X85_Y12_N27
\passeData|ClockTempo|Add0~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~65_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(29) ) + ( GND ) + ( \passeData|ClockTempo|Add0~14\ ))
-- \passeData|ClockTempo|Add0~66\ = CARRY(( \passeData|ClockTempo|Iteration\(29) ) + ( GND ) + ( \passeData|ClockTempo|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(29),
	cin => \passeData|ClockTempo|Add0~14\,
	sumout => \passeData|ClockTempo|Add0~65_sumout\,
	cout => \passeData|ClockTempo|Add0~66\);

-- Location: FF_X85_Y12_N29
\passeData|ClockTempo|Iteration[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~65_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(29));

-- Location: LABCELL_X85_Y12_N30
\passeData|ClockTempo|Add0~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~73_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(30) ) + ( GND ) + ( \passeData|ClockTempo|Add0~66\ ))
-- \passeData|ClockTempo|Add0~74\ = CARRY(( \passeData|ClockTempo|Iteration\(30) ) + ( GND ) + ( \passeData|ClockTempo|Add0~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(30),
	cin => \passeData|ClockTempo|Add0~66\,
	sumout => \passeData|ClockTempo|Add0~73_sumout\,
	cout => \passeData|ClockTempo|Add0~74\);

-- Location: FF_X85_Y12_N32
\passeData|ClockTempo|Iteration[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~73_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(30));

-- Location: LABCELL_X85_Y12_N33
\passeData|ClockTempo|Add0~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ClockTempo|Add0~69_sumout\ = SUM(( \passeData|ClockTempo|Iteration\(31) ) + ( GND ) + ( \passeData|ClockTempo|Add0~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(31),
	cin => \passeData|ClockTempo|Add0~74\,
	sumout => \passeData|ClockTempo|Add0~69_sumout\);

-- Location: FF_X85_Y12_N35
\passeData|ClockTempo|Iteration[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|ClockTempo|Add0~69_sumout\,
	clrn => \control|EA.E0~q\,
	sclr => \passeData|ClockTempo|LessThan0~11_combout\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|ClockTempo|Iteration\(31));

-- Location: LABCELL_X85_Y12_N51
\passeData|ChecaSeLimiteTempo|Equal0~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~7_combout\ = ( !\passeData|ClockTempo|Iteration\(31) & ( (!\passeData|ClockTempo|Iteration\(30) & !\passeData|ClockTempo|Iteration\(29)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(30),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(29),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(31),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~7_combout\);

-- Location: MLABCELL_X84_Y12_N39
\passeData|ChecaSeLimiteTempo|Equal0~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~10_combout\ = ( \passeData|ClockTempo|Iteration\(10) & ( (!\passeData|ClockTempo|Iteration\(8) & (!\passeData|registradorNivel|B\(1) & (!\passeData|ClockTempo|Iteration\(12) & !\passeData|registradorNivel|B\(0)))) ) ) 
-- # ( !\passeData|ClockTempo|Iteration\(10) & ( (!\passeData|ClockTempo|Iteration\(12) & (\passeData|registradorNivel|B\(0) & (!\passeData|ClockTempo|Iteration\(8) $ (!\passeData|registradorNivel|B\(1))))) # (\passeData|ClockTempo|Iteration\(12) & 
-- (\passeData|ClockTempo|Iteration\(8) & (\passeData|registradorNivel|B\(1) & !\passeData|registradorNivel|B\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000101100000000000010110000010000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(8),
	datab => \passeData|registradorNivel|ALT_INV_B\(1),
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(12),
	datad => \passeData|registradorNivel|ALT_INV_B\(0),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(10),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~10_combout\);

-- Location: MLABCELL_X84_Y13_N54
\passeData|ChecaSeLimiteTempo|Equal0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~8_combout\ = ( \passeData|ClockTempo|Iteration\(3) & ( (!\passeData|ClockTempo|Iteration\(11) & \passeData|ClockTempo|Iteration\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(11),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(2),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(3),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~8_combout\);

-- Location: MLABCELL_X84_Y13_N57
\passeData|ChecaSeLimiteTempo|Equal0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~9_combout\ = ( \passeData|ClockTempo|Iteration\(1) & ( (\passeData|ClockTempo|Iteration\(6) & (\passeData|ClockTempo|Iteration\(4) & (\passeData|ClockTempo|Iteration\(5) & \passeData|ClockTempo|Iteration\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(6),
	datab => \passeData|ClockTempo|ALT_INV_Iteration\(4),
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(5),
	datad => \passeData|ClockTempo|ALT_INV_Iteration\(0),
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(1),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~9_combout\);

-- Location: MLABCELL_X84_Y13_N6
\passeData|ChecaSeLimiteTempo|Equal0~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~11_combout\ = ( \passeData|ChecaSeLimiteTempo|Equal0~8_combout\ & ( \passeData|ChecaSeLimiteTempo|Equal0~9_combout\ & ( (!\passeData|ClockTempo|Iteration\(9) & (\passeData|ChecaSeLimiteTempo|Equal0~10_combout\ & 
-- (!\passeData|ClockTempo|Iteration\(7) $ (!\passeData|resultadoUltimo|Equal2~1_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000001100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ClockTempo|ALT_INV_Iteration\(7),
	datab => \passeData|resultadoUltimo|ALT_INV_Equal2~1_combout\,
	datac => \passeData|ClockTempo|ALT_INV_Iteration\(9),
	datad => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~10_combout\,
	datae => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~8_combout\,
	dataf => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~9_combout\,
	combout => \passeData|ChecaSeLimiteTempo|Equal0~11_combout\);

-- Location: MLABCELL_X84_Y12_N24
\passeData|ChecaSeLimiteTempo|Equal0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~4_combout\ = ( \passeData|ClockTempo|Iteration\(13) & ( (\passeData|tempo|Op[26]~0_combout\ & \passeData|ChecaSeLimiteTempo|Equal0~3_combout\) ) ) # ( !\passeData|ClockTempo|Iteration\(13) & ( 
-- (!\passeData|tempo|Op[26]~0_combout\ & \passeData|ChecaSeLimiteTempo|Equal0~3_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|tempo|ALT_INV_Op[26]~0_combout\,
	datad => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~3_combout\,
	dataf => \passeData|ClockTempo|ALT_INV_Iteration\(13),
	combout => \passeData|ChecaSeLimiteTempo|Equal0~4_combout\);

-- Location: MLABCELL_X84_Y12_N30
\passeData|ChecaSeLimiteTempo|Equal0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaSeLimiteTempo|Equal0~combout\ = LCELL(( \passeData|ChecaSeLimiteTempo|Equal0~4_combout\ & ( (\passeData|ChecaSeLimiteTempo|Equal0~7_combout\ & (\passeData|ChecaSeLimiteTempo|Equal0~2_combout\ & 
-- (\passeData|ChecaSeLimiteTempo|Equal0~11_combout\ & \passeData|ChecaSeLimiteTempo|Equal0~6_combout\))) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~7_combout\,
	datab => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~2_combout\,
	datac => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~11_combout\,
	datad => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~6_combout\,
	dataf => \passeData|ChecaSeLimiteTempo|ALT_INV_Equal0~4_combout\,
	combout => \passeData|ChecaSeLimiteTempo|Equal0~combout\);

-- Location: MLABCELL_X84_Y10_N54
\passeData|contaIteracoes|Iteration[2]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|contaIteracoes|Iteration[2]~1_combout\ = ( \control|EA.E2~q\ & ( !\passeData|contaIteracoes|Iteration\(2) $ (((!\passeData|contaIteracoes|Iteration\(0)) # (!\passeData|contaIteracoes|Iteration\(1)))) ) ) # ( !\control|EA.E2~q\ & ( 
-- \passeData|contaIteracoes|Iteration\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100010001111011100001000111101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	dataf => \control|ALT_INV_EA.E2~q\,
	combout => \passeData|contaIteracoes|Iteration[2]~1_combout\);

-- Location: FF_X83_Y10_N5
\passeData|contaIteracoes|Iteration[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \passeData|ChecaSeLimiteTempo|Equal0~combout\,
	asdata => \passeData|contaIteracoes|Iteration[2]~1_combout\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|contaIteracoes|Iteration\(2));

-- Location: IOIBUF_X12_Y0_N18
\SW[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: FF_X84_Y10_N8
\passeData|registradorMemoria|B[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \SW[0]~input_o\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|registradorMemoria|B\(0));

-- Location: IOIBUF_X16_Y0_N1
\SW[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: FF_X84_Y10_N47
\passeData|registradorMemoria|B[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \SW[1]~input_o\,
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|registradorMemoria|B\(1));

-- Location: LABCELL_X83_Y10_N18
\passeData|encontraResposta|Op[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[1]~0_combout\ = ( \passeData|registradorMemoria|B\(0) & ( \passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(1) & (!\passeData|contaIteracoes|Iteration\(2) & 
-- (\passeData|contaIteracoes|Iteration\(3) & \passeData|contaIteracoes|Iteration\(0)))) # (\passeData|contaIteracoes|Iteration\(1) & (!\passeData|contaIteracoes|Iteration\(3) & ((!\passeData|contaIteracoes|Iteration\(0)) # 
-- (\passeData|contaIteracoes|Iteration\(2))))) ) ) ) # ( !\passeData|registradorMemoria|B\(0) & ( \passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(3) & ((!\passeData|contaIteracoes|Iteration\(1)) # 
-- (\passeData|contaIteracoes|Iteration\(2)))) ) ) ) # ( \passeData|registradorMemoria|B\(0) & ( !\passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(3) & ((!\passeData|contaIteracoes|Iteration\(2) & 
-- (!\passeData|contaIteracoes|Iteration\(1) & !\passeData|contaIteracoes|Iteration\(0))) # (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(1) $ (!\passeData|contaIteracoes|Iteration\(0)))))) ) ) ) # ( 
-- !\passeData|registradorMemoria|B\(0) & ( !\passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(2) & ((!\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(1))) # 
-- (\passeData|contaIteracoes|Iteration\(0) & ((!\passeData|contaIteracoes|Iteration\(3)))))) # (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(3) & (!\passeData|contaIteracoes|Iteration\(1) $ 
-- (\passeData|contaIteracoes|Iteration\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100100010110000100100000100000011010000110100000011000000011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datae => \passeData|registradorMemoria|ALT_INV_B\(0),
	dataf => \passeData|registradorMemoria|ALT_INV_B\(1),
	combout => \passeData|encontraResposta|Op[1]~0_combout\);

-- Location: LABCELL_X83_Y10_N0
\passeData|encontraResposta|Op[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[3]~1_combout\ = ( \passeData|registradorMemoria|B\(0) & ( \passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(2) & ((!\passeData|contaIteracoes|Iteration\(1)) # 
-- ((!\passeData|contaIteracoes|Iteration\(3) & !\passeData|contaIteracoes|Iteration\(0))))) # (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(3) & ((\passeData|contaIteracoes|Iteration\(0)) # 
-- (\passeData|contaIteracoes|Iteration\(1))))) ) ) ) # ( !\passeData|registradorMemoria|B\(0) & ( \passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(3)) # ((!\passeData|contaIteracoes|Iteration\(2) & 
-- !\passeData|contaIteracoes|Iteration\(1))) ) ) ) # ( \passeData|registradorMemoria|B\(0) & ( !\passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(3) & (\passeData|contaIteracoes|Iteration\(1) & 
-- ((!\passeData|contaIteracoes|Iteration\(2)) # (\passeData|contaIteracoes|Iteration\(0))))) # (\passeData|contaIteracoes|Iteration\(3) & (!\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(1) & 
-- \passeData|contaIteracoes|Iteration\(0)))) ) ) ) # ( !\passeData|registradorMemoria|B\(0) & ( !\passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(1) & ((!\passeData|contaIteracoes|Iteration\(2) & 
-- (\passeData|contaIteracoes|Iteration\(3) & \passeData|contaIteracoes|Iteration\(0))) # (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(3))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001100000000010000010110011101100111011001010110011100100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datae => \passeData|registradorMemoria|ALT_INV_B\(0),
	dataf => \passeData|registradorMemoria|ALT_INV_B\(1),
	combout => \passeData|encontraResposta|Op[3]~1_combout\);

-- Location: MLABCELL_X84_Y10_N33
\passeData|encontraResposta|Op[0]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[0]~2_combout\ = ( !\passeData|contaIteracoes|Iteration\(2) & ( \passeData|contaIteracoes|Iteration\(3) & ( (!\passeData|contaIteracoes|Iteration\(1) & ((!\passeData|registradorMemoria|B\(0) & 
-- ((!\passeData|registradorMemoria|B\(1)) # (\passeData|contaIteracoes|Iteration\(0)))) # (\passeData|registradorMemoria|B\(0) & (!\passeData|registradorMemoria|B\(1) & \passeData|contaIteracoes|Iteration\(0))))) ) ) ) # ( 
-- \passeData|contaIteracoes|Iteration\(2) & ( !\passeData|contaIteracoes|Iteration\(3) & ( (!\passeData|registradorMemoria|B\(0) & (((!\passeData|registradorMemoria|B\(1)) # (\passeData|contaIteracoes|Iteration\(0))))) # (\passeData|registradorMemoria|B\(0) 
-- & ((!\passeData|registradorMemoria|B\(1) $ (\passeData|contaIteracoes|Iteration\(0))) # (\passeData|contaIteracoes|Iteration\(1)))) ) ) ) # ( !\passeData|contaIteracoes|Iteration\(2) & ( !\passeData|contaIteracoes|Iteration\(3) & ( 
-- (!\passeData|registradorMemoria|B\(0) & (((\passeData|contaIteracoes|Iteration\(1) & !\passeData|registradorMemoria|B\(1))) # (\passeData|contaIteracoes|Iteration\(0)))) # (\passeData|registradorMemoria|B\(0) & ((!\passeData|contaIteracoes|Iteration\(1)) 
-- # ((!\passeData|registradorMemoria|B\(1) & \passeData|contaIteracoes|Iteration\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110001011111110111100011101111110000000101010000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datab => \passeData|registradorMemoria|ALT_INV_B\(0),
	datac => \passeData|registradorMemoria|ALT_INV_B\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datae => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|encontraResposta|Op[0]~2_combout\);

-- Location: MLABCELL_X84_Y10_N39
\passeData|encontraResposta|Op[2]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[2]~3_combout\ = ( \passeData|contaIteracoes|Iteration\(2) & ( \passeData|registradorMemoria|B\(0) & ( (!\passeData|contaIteracoes|Iteration\(3) & ((!\passeData|contaIteracoes|Iteration\(0)) # 
-- ((!\passeData|contaIteracoes|Iteration\(1) & \passeData|registradorMemoria|B\(1))))) ) ) ) # ( !\passeData|contaIteracoes|Iteration\(2) & ( \passeData|registradorMemoria|B\(0) & ( (!\passeData|contaIteracoes|Iteration\(0) & 
-- (\passeData|registradorMemoria|B\(1) & (!\passeData|contaIteracoes|Iteration\(1) $ (!\passeData|contaIteracoes|Iteration\(3))))) # (\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(3) & 
-- ((!\passeData|contaIteracoes|Iteration\(1)) # (!\passeData|registradorMemoria|B\(1))))) ) ) ) # ( \passeData|contaIteracoes|Iteration\(2) & ( !\passeData|registradorMemoria|B\(0) & ( (!\passeData|contaIteracoes|Iteration\(1) & 
-- (!\passeData|contaIteracoes|Iteration\(3) & ((\passeData|registradorMemoria|B\(1)) # (\passeData|contaIteracoes|Iteration\(0))))) ) ) ) # ( !\passeData|contaIteracoes|Iteration\(2) & ( !\passeData|registradorMemoria|B\(0) & ( 
-- !\passeData|contaIteracoes|Iteration\(1) $ (!\passeData|contaIteracoes|Iteration\(3)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001111001100010011000000000001010110000010001010111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|registradorMemoria|ALT_INV_B\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datae => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	dataf => \passeData|registradorMemoria|ALT_INV_B\(0),
	combout => \passeData|encontraResposta|Op[2]~3_combout\);

-- Location: LABCELL_X85_Y10_N3
\passeData|motraCasaZeroJogo|F[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|motraCasaZeroJogo|F[0]~0_combout\ = ( \passeData|encontraResposta|Op[0]~2_combout\ & ( \passeData|encontraResposta|Op[2]~3_combout\ & ( (!\passeData|encontraResposta|Op[1]~0_combout\ & \passeData|encontraResposta|Op[3]~1_combout\) ) ) ) # ( 
-- !\passeData|encontraResposta|Op[0]~2_combout\ & ( \passeData|encontraResposta|Op[2]~3_combout\ & ( (!\passeData|encontraResposta|Op[1]~0_combout\ & !\passeData|encontraResposta|Op[3]~1_combout\) ) ) ) # ( \passeData|encontraResposta|Op[0]~2_combout\ & ( 
-- !\passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[1]~0_combout\ $ (\passeData|encontraResposta|Op[3]~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101001011010010110100000101000000000101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	datae => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	combout => \passeData|motraCasaZeroJogo|F[0]~0_combout\);

-- Location: FF_X85_Y10_N4
\passeData|mostraRegZero|B[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|motraCasaZeroJogo|F[0]~0_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegZero|B\(0));

-- Location: LABCELL_X85_Y10_N57
\passeData|motraCasaZeroJogo|F[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|motraCasaZeroJogo|F[1]~1_combout\ = ( \passeData|encontraResposta|Op[0]~2_combout\ & ( \passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[1]~0_combout\ $ (\passeData|encontraResposta|Op[3]~1_combout\) ) ) ) # ( 
-- !\passeData|encontraResposta|Op[0]~2_combout\ & ( \passeData|encontraResposta|Op[2]~3_combout\ & ( (\passeData|encontraResposta|Op[3]~1_combout\) # (\passeData|encontraResposta|Op[1]~0_combout\) ) ) ) # ( \passeData|encontraResposta|Op[0]~2_combout\ & ( 
-- !\passeData|encontraResposta|Op[2]~3_combout\ & ( (\passeData|encontraResposta|Op[1]~0_combout\ & \passeData|encontraResposta|Op[3]~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010101011111010111111010010110100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	datae => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	combout => \passeData|motraCasaZeroJogo|F[1]~1_combout\);

-- Location: FF_X85_Y10_N58
\passeData|mostraRegZero|B[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|motraCasaZeroJogo|F[1]~1_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegZero|B\(1));

-- Location: LABCELL_X85_Y10_N48
\passeData|motraCasaZeroJogo|F[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|motraCasaZeroJogo|F[2]~2_combout\ = ( \passeData|encontraResposta|Op[2]~3_combout\ & ( \passeData|encontraResposta|Op[0]~2_combout\ & ( (\passeData|encontraResposta|Op[1]~0_combout\ & \passeData|encontraResposta|Op[3]~1_combout\) ) ) ) # ( 
-- \passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( \passeData|encontraResposta|Op[3]~1_combout\ ) ) ) # ( !\passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( 
-- (\passeData|encontraResposta|Op[1]~0_combout\ & !\passeData|encontraResposta|Op[3]~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000000000001111111100000000000000000000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	datae => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	combout => \passeData|motraCasaZeroJogo|F[2]~2_combout\);

-- Location: FF_X85_Y10_N49
\passeData|mostraRegZero|B[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|motraCasaZeroJogo|F[2]~2_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegZero|B\(2));

-- Location: LABCELL_X85_Y10_N18
\passeData|motraCasaZeroJogo|F[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|motraCasaZeroJogo|F[3]~3_combout\ = ( \passeData|encontraResposta|Op[2]~3_combout\ & ( \passeData|encontraResposta|Op[0]~2_combout\ & ( \passeData|encontraResposta|Op[1]~0_combout\ ) ) ) # ( !\passeData|encontraResposta|Op[2]~3_combout\ & ( 
-- \passeData|encontraResposta|Op[0]~2_combout\ & ( (!\passeData|encontraResposta|Op[3]~1_combout\ & !\passeData|encontraResposta|Op[1]~0_combout\) ) ) ) # ( \passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( 
-- (!\passeData|encontraResposta|Op[3]~1_combout\ & !\passeData|encontraResposta|Op[1]~0_combout\) ) ) ) # ( !\passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( (\passeData|encontraResposta|Op[3]~1_combout\ & 
-- \passeData|encontraResposta|Op[1]~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011110011000000000011001100000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	datae => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	combout => \passeData|motraCasaZeroJogo|F[3]~3_combout\);

-- Location: FF_X85_Y10_N19
\passeData|mostraRegZero|B[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|motraCasaZeroJogo|F[3]~3_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegZero|B\(3));

-- Location: LABCELL_X85_Y10_N39
\passeData|motraCasaZeroJogo|F[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|motraCasaZeroJogo|F[4]~4_combout\ = ( \passeData|encontraResposta|Op[2]~3_combout\ & ( \passeData|encontraResposta|Op[0]~2_combout\ & ( !\passeData|encontraResposta|Op[3]~1_combout\ ) ) ) # ( !\passeData|encontraResposta|Op[2]~3_combout\ & ( 
-- \passeData|encontraResposta|Op[0]~2_combout\ & ( (!\passeData|encontraResposta|Op[3]~1_combout\) # (!\passeData|encontraResposta|Op[1]~0_combout\) ) ) ) # ( \passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( 
-- (!\passeData|encontraResposta|Op[3]~1_combout\ & !\passeData|encontraResposta|Op[1]~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110000001100000011111100111111001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	datae => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	combout => \passeData|motraCasaZeroJogo|F[4]~4_combout\);

-- Location: FF_X85_Y10_N40
\passeData|mostraRegZero|B[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|motraCasaZeroJogo|F[4]~4_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegZero|B\(4));

-- Location: LABCELL_X85_Y10_N30
\passeData|motraCasaZeroJogo|F[5]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|motraCasaZeroJogo|F[5]~5_combout\ = ( \passeData|encontraResposta|Op[2]~3_combout\ & ( (\passeData|encontraResposta|Op[0]~2_combout\ & (!\passeData|encontraResposta|Op[1]~0_combout\ $ (!\passeData|encontraResposta|Op[3]~1_combout\))) ) ) # ( 
-- !\passeData|encontraResposta|Op[2]~3_combout\ & ( (!\passeData|encontraResposta|Op[3]~1_combout\ & ((\passeData|encontraResposta|Op[0]~2_combout\) # (\passeData|encontraResposta|Op[1]~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000011110000001100001111000000000000001111000000000000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	combout => \passeData|motraCasaZeroJogo|F[5]~5_combout\);

-- Location: FF_X85_Y10_N31
\passeData|mostraRegZero|B[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|motraCasaZeroJogo|F[5]~5_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegZero|B\(5));

-- Location: LABCELL_X85_Y10_N24
\passeData|motraCasaZeroJogo|F[6]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|motraCasaZeroJogo|F[6]~6_combout\ = ( \passeData|encontraResposta|Op[2]~3_combout\ & ( \passeData|encontraResposta|Op[0]~2_combout\ & ( (!\passeData|encontraResposta|Op[3]~1_combout\ & \passeData|encontraResposta|Op[1]~0_combout\) ) ) ) # ( 
-- !\passeData|encontraResposta|Op[2]~3_combout\ & ( \passeData|encontraResposta|Op[0]~2_combout\ & ( (!\passeData|encontraResposta|Op[3]~1_combout\ & !\passeData|encontraResposta|Op[1]~0_combout\) ) ) ) # ( \passeData|encontraResposta|Op[2]~3_combout\ & ( 
-- !\passeData|encontraResposta|Op[0]~2_combout\ & ( (\passeData|encontraResposta|Op[3]~1_combout\ & !\passeData|encontraResposta|Op[1]~0_combout\) ) ) ) # ( !\passeData|encontraResposta|Op[2]~3_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( 
-- (!\passeData|encontraResposta|Op[3]~1_combout\ & !\passeData|encontraResposta|Op[1]~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110000000000001100110000000011001100000000000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	datae => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	combout => \passeData|motraCasaZeroJogo|F[6]~6_combout\);

-- Location: FF_X85_Y10_N25
\passeData|mostraRegZero|B[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|motraCasaZeroJogo|F[6]~6_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegZero|B\(6));

-- Location: LABCELL_X83_Y10_N45
\passeData|a2|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|a2|Mux2~0_combout\ = ( \passeData|contaIteracoes|Iteration\(1) & ( (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(0) & !\passeData|contaIteracoes|Iteration\(3))) ) ) # ( 
-- !\passeData|contaIteracoes|Iteration\(1) & ( (\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(2) $ (!\passeData|contaIteracoes|Iteration\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100001010000001010000101001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	combout => \passeData|a2|Mux2~0_combout\);

-- Location: LABCELL_X83_Y10_N24
\passeData|a4|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|a4|Mux2~0_combout\ = ( \passeData|contaIteracoes|Iteration\(3) & ( (!\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(1) & !\passeData|contaIteracoes|Iteration\(2))) ) ) # ( 
-- !\passeData|contaIteracoes|Iteration\(3) & ( (\passeData|contaIteracoes|Iteration\(0) & ((!\passeData|contaIteracoes|Iteration\(2)) # (\passeData|contaIteracoes|Iteration\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000011001100110000001111000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|a4|Mux2~0_combout\);

-- Location: MLABCELL_X84_Y10_N12
\passeData|encontraResposta|Op[5]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[5]~6_combout\ = ( \passeData|a4|Mux2~0_combout\ & ( \passeData|a3|Mux2~0_combout\ & ( (!\passeData|registradorMemoria|B\(0) & (((!\passeData|contaIteracoes|Iteration\(3) & \passeData|registradorMemoria|B\(1))))) # 
-- (\passeData|registradorMemoria|B\(0) & (((\passeData|registradorMemoria|B\(1))) # (\passeData|a2|Mux2~0_combout\))) ) ) ) # ( !\passeData|a4|Mux2~0_combout\ & ( \passeData|a3|Mux2~0_combout\ & ( (!\passeData|registradorMemoria|B\(0) & 
-- (((!\passeData|contaIteracoes|Iteration\(3) & \passeData|registradorMemoria|B\(1))))) # (\passeData|registradorMemoria|B\(0) & (\passeData|a2|Mux2~0_combout\ & ((!\passeData|registradorMemoria|B\(1))))) ) ) ) # ( \passeData|a4|Mux2~0_combout\ & ( 
-- !\passeData|a3|Mux2~0_combout\ & ( (\passeData|registradorMemoria|B\(0) & ((\passeData|registradorMemoria|B\(1)) # (\passeData|a2|Mux2~0_combout\))) ) ) ) # ( !\passeData|a4|Mux2~0_combout\ & ( !\passeData|a3|Mux2~0_combout\ & ( 
-- (\passeData|a2|Mux2~0_combout\ & (\passeData|registradorMemoria|B\(0) & !\passeData|registradorMemoria|B\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100000000000100010011001100010001110000000001000111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|a2|ALT_INV_Mux2~0_combout\,
	datab => \passeData|registradorMemoria|ALT_INV_B\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datad => \passeData|registradorMemoria|ALT_INV_B\(1),
	datae => \passeData|a4|ALT_INV_Mux2~0_combout\,
	dataf => \passeData|a3|ALT_INV_Mux2~0_combout\,
	combout => \passeData|encontraResposta|Op[5]~6_combout\);

-- Location: MLABCELL_X84_Y10_N18
\passeData|a3|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|a3|Mux1~0_combout\ = ( \passeData|contaIteracoes|Iteration\(2) & ( (!\passeData|contaIteracoes|Iteration\(3) & ((!\passeData|contaIteracoes|Iteration\(1)) # (!\passeData|contaIteracoes|Iteration\(0)))) ) ) # ( 
-- !\passeData|contaIteracoes|Iteration\(2) & ( (\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(1) $ (!\passeData|contaIteracoes|Iteration\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100001100000000110000110011111100000000001111110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	combout => \passeData|a3|Mux1~0_combout\);

-- Location: LABCELL_X83_Y10_N39
\passeData|a2|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|a2|Mux1~0_combout\ = ( \passeData|contaIteracoes|Iteration\(3) & ( (!\passeData|contaIteracoes|Iteration\(2) & (\passeData|contaIteracoes|Iteration\(0) & !\passeData|contaIteracoes|Iteration\(1))) ) ) # ( 
-- !\passeData|contaIteracoes|Iteration\(3) & ( (\passeData|contaIteracoes|Iteration\(2) & (\passeData|contaIteracoes|Iteration\(0) & \passeData|contaIteracoes|Iteration\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010100001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|a2|Mux1~0_combout\);

-- Location: MLABCELL_X84_Y10_N3
\passeData|a4|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|a4|Mux1~0_combout\ = ( \passeData|contaIteracoes|Iteration\(2) & ( (\passeData|contaIteracoes|Iteration\(0) & !\passeData|contaIteracoes|Iteration\(3)) ) ) # ( !\passeData|contaIteracoes|Iteration\(2) & ( 
-- (!\passeData|contaIteracoes|Iteration\(0) & (\passeData|contaIteracoes|Iteration\(1) & !\passeData|contaIteracoes|Iteration\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000000000001000100000000001010101000000000101010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	combout => \passeData|a4|Mux1~0_combout\);

-- Location: MLABCELL_X84_Y10_N24
\passeData|encontraResposta|Op[6]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[6]~7_combout\ = ( \passeData|a4|Mux1~0_combout\ & ( (!\passeData|registradorMemoria|B\(0) & (\passeData|a3|Mux1~0_combout\ & ((\passeData|registradorMemoria|B\(1))))) # (\passeData|registradorMemoria|B\(0) & 
-- (((\passeData|registradorMemoria|B\(1)) # (\passeData|a2|Mux1~0_combout\)))) ) ) # ( !\passeData|a4|Mux1~0_combout\ & ( (!\passeData|registradorMemoria|B\(0) & (\passeData|a3|Mux1~0_combout\ & ((\passeData|registradorMemoria|B\(1))))) # 
-- (\passeData|registradorMemoria|B\(0) & (((\passeData|a2|Mux1~0_combout\ & !\passeData|registradorMemoria|B\(1))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001101000100000000110100010000000011011101110000001101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|a3|ALT_INV_Mux1~0_combout\,
	datab => \passeData|registradorMemoria|ALT_INV_B\(0),
	datac => \passeData|a2|ALT_INV_Mux1~0_combout\,
	datad => \passeData|registradorMemoria|ALT_INV_B\(1),
	dataf => \passeData|a4|ALT_INV_Mux1~0_combout\,
	combout => \passeData|encontraResposta|Op[6]~7_combout\);

-- Location: LABCELL_X83_Y10_N6
\passeData|encontraResposta|Op[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[4]~5_combout\ = ( \passeData|registradorMemoria|B\(0) & ( \passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(1) & ((!\passeData|contaIteracoes|Iteration\(0) & 
-- ((!\passeData|contaIteracoes|Iteration\(3)))) # (\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(2))))) ) ) ) # ( !\passeData|registradorMemoria|B\(0) & ( \passeData|registradorMemoria|B\(1) & ( 
-- (!\passeData|contaIteracoes|Iteration\(0) & ((!\passeData|contaIteracoes|Iteration\(3)) # ((!\passeData|contaIteracoes|Iteration\(2) & !\passeData|contaIteracoes|Iteration\(1))))) ) ) ) # ( \passeData|registradorMemoria|B\(0) & ( 
-- !\passeData|registradorMemoria|B\(1) & ( (!\passeData|contaIteracoes|Iteration\(1) & (!\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(2) $ (!\passeData|contaIteracoes|Iteration\(3))))) # 
-- (\passeData|contaIteracoes|Iteration\(1) & (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(3)))) ) ) ) # ( !\passeData|registradorMemoria|B\(0) & ( !\passeData|registradorMemoria|B\(1) & ( 
-- (!\passeData|contaIteracoes|Iteration\(2) & (\passeData|contaIteracoes|Iteration\(3) & !\passeData|contaIteracoes|Iteration\(1))) # (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(3) & 
-- \passeData|contaIteracoes|Iteration\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010010000100100011001000000010011101100000000001100000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datae => \passeData|registradorMemoria|ALT_INV_B\(0),
	dataf => \passeData|registradorMemoria|ALT_INV_B\(1),
	combout => \passeData|encontraResposta|Op[4]~5_combout\);

-- Location: LABCELL_X83_Y10_N27
\passeData|a3|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|a3|Mux0~0_combout\ = ( \passeData|contaIteracoes|Iteration\(3) & ( (!\passeData|contaIteracoes|Iteration\(2) & !\passeData|contaIteracoes|Iteration\(1)) ) ) # ( !\passeData|contaIteracoes|Iteration\(3) & ( 
-- (\passeData|contaIteracoes|Iteration\(2) & (\passeData|contaIteracoes|Iteration\(0) & \passeData|contaIteracoes|Iteration\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010110101010000000001010101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|a3|Mux0~0_combout\);

-- Location: MLABCELL_X84_Y10_N48
\passeData|encontraResposta|Op[7]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[7]~4_combout\ = ( \passeData|contaIteracoes|Iteration\(0) & ( (!\passeData|registradorMemoria|B\(0) & (\passeData|a3|Mux0~0_combout\ & ((\passeData|registradorMemoria|B\(1))))) # (\passeData|registradorMemoria|B\(0) & 
-- (((\passeData|compDez|Equal0~0_combout\)))) ) ) # ( !\passeData|contaIteracoes|Iteration\(0) & ( (!\passeData|registradorMemoria|B\(0) & (\passeData|a3|Mux0~0_combout\ & ((\passeData|registradorMemoria|B\(1))))) # (\passeData|registradorMemoria|B\(0) & 
-- (((\passeData|compDez|Equal0~0_combout\ & !\passeData|registradorMemoria|B\(1))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001101000100000000110100010000000011010001110000001101000111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|a3|ALT_INV_Mux0~0_combout\,
	datab => \passeData|registradorMemoria|ALT_INV_B\(0),
	datac => \passeData|compDez|ALT_INV_Equal0~0_combout\,
	datad => \passeData|registradorMemoria|ALT_INV_B\(1),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	combout => \passeData|encontraResposta|Op[7]~4_combout\);

-- Location: LABCELL_X85_Y10_N33
\passeData|mostraCasaUmJogo|F[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|mostraCasaUmJogo|F[0]~0_combout\ = ( \passeData|encontraResposta|Op[7]~4_combout\ & ( (\passeData|encontraResposta|Op[4]~5_combout\ & (!\passeData|encontraResposta|Op[5]~6_combout\ $ (!\passeData|encontraResposta|Op[6]~7_combout\))) ) ) # ( 
-- !\passeData|encontraResposta|Op[7]~4_combout\ & ( (!\passeData|encontraResposta|Op[5]~6_combout\ & (!\passeData|encontraResposta|Op[6]~7_combout\ $ (!\passeData|encontraResposta|Op[4]~5_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101010100000000010101010000000000000010110100000000001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\,
	combout => \passeData|mostraCasaUmJogo|F[0]~0_combout\);

-- Location: FF_X85_Y10_N34
\passeData|mostraRegUm|B[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|mostraCasaUmJogo|F[0]~0_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegUm|B\(0));

-- Location: LABCELL_X85_Y10_N42
\passeData|mostraCasaUmJogo|F[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|mostraCasaUmJogo|F[1]~1_combout\ = ( \passeData|encontraResposta|Op[5]~6_combout\ & ( (!\passeData|encontraResposta|Op[4]~5_combout\ & (\passeData|encontraResposta|Op[6]~7_combout\)) # (\passeData|encontraResposta|Op[4]~5_combout\ & 
-- ((\passeData|encontraResposta|Op[7]~4_combout\))) ) ) # ( !\passeData|encontraResposta|Op[5]~6_combout\ & ( (\passeData|encontraResposta|Op[6]~7_combout\ & (!\passeData|encontraResposta|Op[4]~5_combout\ $ (!\passeData|encontraResposta|Op[7]~4_combout\))) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100110000000000110011000000110000001111110011000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\,
	combout => \passeData|mostraCasaUmJogo|F[1]~1_combout\);

-- Location: FF_X85_Y10_N43
\passeData|mostraRegUm|B[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|mostraCasaUmJogo|F[1]~1_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegUm|B\(1));

-- Location: LABCELL_X85_Y10_N12
\passeData|mostraCasaUmJogo|F[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|mostraCasaUmJogo|F[2]~2_combout\ = ( \passeData|encontraResposta|Op[5]~6_combout\ & ( (!\passeData|encontraResposta|Op[6]~7_combout\ & (!\passeData|encontraResposta|Op[4]~5_combout\ & !\passeData|encontraResposta|Op[7]~4_combout\)) # 
-- (\passeData|encontraResposta|Op[6]~7_combout\ & ((\passeData|encontraResposta|Op[7]~4_combout\))) ) ) # ( !\passeData|encontraResposta|Op[5]~6_combout\ & ( (\passeData|encontraResposta|Op[6]~7_combout\ & (!\passeData|encontraResposta|Op[4]~5_combout\ & 
-- \passeData|encontraResposta|Op[7]~4_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000011000011000000001100111100000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\,
	combout => \passeData|mostraCasaUmJogo|F[2]~2_combout\);

-- Location: FF_X85_Y10_N13
\passeData|mostraRegUm|B[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|mostraCasaUmJogo|F[2]~2_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegUm|B\(2));

-- Location: LABCELL_X85_Y10_N15
\passeData|mostraCasaUmJogo|F[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|mostraCasaUmJogo|F[3]~3_combout\ = ( \passeData|encontraResposta|Op[7]~4_combout\ & ( (\passeData|encontraResposta|Op[5]~6_combout\ & (!\passeData|encontraResposta|Op[6]~7_combout\ $ (\passeData|encontraResposta|Op[4]~5_combout\))) ) ) # ( 
-- !\passeData|encontraResposta|Op[7]~4_combout\ & ( (!\passeData|encontraResposta|Op[5]~6_combout\ & (!\passeData|encontraResposta|Op[6]~7_combout\ $ (!\passeData|encontraResposta|Op[4]~5_combout\))) # (\passeData|encontraResposta|Op[5]~6_combout\ & 
-- (\passeData|encontraResposta|Op[6]~7_combout\ & \passeData|encontraResposta|Op[4]~5_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101010100101000010101010010101010000000001010101000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\,
	combout => \passeData|mostraCasaUmJogo|F[3]~3_combout\);

-- Location: FF_X85_Y10_N16
\passeData|mostraRegUm|B[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|mostraCasaUmJogo|F[3]~3_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegUm|B\(3));

-- Location: LABCELL_X85_Y10_N6
\passeData|mostraCasaUmJogo|F[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|mostraCasaUmJogo|F[4]~4_combout\ = ( \passeData|encontraResposta|Op[7]~4_combout\ & ( (\passeData|encontraResposta|Op[4]~5_combout\ & (!\passeData|encontraResposta|Op[6]~7_combout\ & !\passeData|encontraResposta|Op[5]~6_combout\)) ) ) # ( 
-- !\passeData|encontraResposta|Op[7]~4_combout\ & ( ((\passeData|encontraResposta|Op[6]~7_combout\ & !\passeData|encontraResposta|Op[5]~6_combout\)) # (\passeData|encontraResposta|Op[4]~5_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100110011001111110011001100110000000000000011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\,
	combout => \passeData|mostraCasaUmJogo|F[4]~4_combout\);

-- Location: FF_X85_Y10_N7
\passeData|mostraRegUm|B[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|mostraCasaUmJogo|F[4]~4_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegUm|B\(4));

-- Location: LABCELL_X85_Y10_N9
\passeData|mostraCasaUmJogo|F[5]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|mostraCasaUmJogo|F[5]~5_combout\ = ( \passeData|encontraResposta|Op[7]~4_combout\ & ( (!\passeData|encontraResposta|Op[5]~6_combout\ & (\passeData|encontraResposta|Op[6]~7_combout\ & \passeData|encontraResposta|Op[4]~5_combout\)) ) ) # ( 
-- !\passeData|encontraResposta|Op[7]~4_combout\ & ( (!\passeData|encontraResposta|Op[5]~6_combout\ & (!\passeData|encontraResposta|Op[6]~7_combout\ & \passeData|encontraResposta|Op[4]~5_combout\)) # (\passeData|encontraResposta|Op[5]~6_combout\ & 
-- ((!\passeData|encontraResposta|Op[6]~7_combout\) # (\passeData|encontraResposta|Op[4]~5_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000011110101010100001111010100000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\,
	combout => \passeData|mostraCasaUmJogo|F[5]~5_combout\);

-- Location: FF_X85_Y10_N10
\passeData|mostraRegUm|B[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|mostraCasaUmJogo|F[5]~5_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegUm|B\(5));

-- Location: LABCELL_X85_Y10_N45
\passeData|mostraCasaUmJogo|F[6]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|mostraCasaUmJogo|F[6]~6_combout\ = ( \passeData|encontraResposta|Op[7]~4_combout\ & ( (!\passeData|encontraResposta|Op[5]~6_combout\ & (\passeData|encontraResposta|Op[6]~7_combout\ & !\passeData|encontraResposta|Op[4]~5_combout\)) ) ) # ( 
-- !\passeData|encontraResposta|Op[7]~4_combout\ & ( (!\passeData|encontraResposta|Op[5]~6_combout\ & (!\passeData|encontraResposta|Op[6]~7_combout\)) # (\passeData|encontraResposta|Op[5]~6_combout\ & (\passeData|encontraResposta|Op[6]~7_combout\ & 
-- \passeData|encontraResposta|Op[4]~5_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100101101000001010010100001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|encontraResposta|ALT_INV_Op[5]~6_combout\,
	datac => \passeData|encontraResposta|ALT_INV_Op[6]~7_combout\,
	datad => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[7]~4_combout\,
	combout => \passeData|mostraCasaUmJogo|F[6]~6_combout\);

-- Location: FF_X85_Y10_N46
\passeData|mostraRegUm|B[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|mostraCasaUmJogo|F[6]~6_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E2~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraRegUm|B\(6));

-- Location: IOIBUF_X2_Y0_N41
\SW[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: IOIBUF_X4_Y0_N52
\SW[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: MLABCELL_X82_Y10_N39
\passeData|ChecaAcerto|Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaAcerto|Equal0~3_combout\ = ( \passeData|encontraResposta|Op[3]~1_combout\ & ( (\SW[3]~input_o\ & (!\SW[4]~input_o\ $ (\passeData|encontraResposta|Op[4]~5_combout\))) ) ) # ( !\passeData|encontraResposta|Op[3]~1_combout\ & ( 
-- (!\SW[3]~input_o\ & (!\SW[4]~input_o\ $ (\passeData|encontraResposta|Op[4]~5_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010010100000000101001010000000000000000101001010000000010100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[4]~input_o\,
	datac => \passeData|encontraResposta|ALT_INV_Op[4]~5_combout\,
	datad => \ALT_INV_SW[3]~input_o\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[3]~1_combout\,
	combout => \passeData|ChecaAcerto|Equal0~3_combout\);

-- Location: IOIBUF_X4_Y0_N1
\SW[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: MLABCELL_X82_Y10_N54
\passeData|ChecaAcerto|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaAcerto|Equal0~2_combout\ = ( \passeData|compDez|Equal0~0_combout\ & ( \passeData|contaIteracoes|Iteration\(0) & ( !\SW[7]~input_o\ $ (((!\passeData|registradorMemoria|B\(0) & ((!\passeData|a3|Mux0~0_combout\) # 
-- (!\passeData|registradorMemoria|B\(1)))))) ) ) ) # ( !\passeData|compDez|Equal0~0_combout\ & ( \passeData|contaIteracoes|Iteration\(0) & ( !\SW[7]~input_o\ $ (((!\passeData|a3|Mux0~0_combout\) # ((!\passeData|registradorMemoria|B\(1)) # 
-- (\passeData|registradorMemoria|B\(0))))) ) ) ) # ( \passeData|compDez|Equal0~0_combout\ & ( !\passeData|contaIteracoes|Iteration\(0) & ( !\SW[7]~input_o\ $ (((!\passeData|registradorMemoria|B\(1) & ((!\passeData|registradorMemoria|B\(0)))) # 
-- (\passeData|registradorMemoria|B\(1) & ((!\passeData|a3|Mux0~0_combout\) # (\passeData|registradorMemoria|B\(0)))))) ) ) ) # ( !\passeData|compDez|Equal0~0_combout\ & ( !\passeData|contaIteracoes|Iteration\(0) & ( !\SW[7]~input_o\ $ 
-- (((!\passeData|a3|Mux0~0_combout\) # ((!\passeData|registradorMemoria|B\(1)) # (\passeData|registradorMemoria|B\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011011000110011001101101100001100110110001100110011011011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|a3|ALT_INV_Mux0~0_combout\,
	datab => \ALT_INV_SW[7]~input_o\,
	datac => \passeData|registradorMemoria|ALT_INV_B\(1),
	datad => \passeData|registradorMemoria|ALT_INV_B\(0),
	datae => \passeData|compDez|ALT_INV_Equal0~0_combout\,
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	combout => \passeData|ChecaAcerto|Equal0~2_combout\);

-- Location: IOIBUF_X4_Y0_N35
\SW[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: MLABCELL_X82_Y10_N24
\passeData|ChecaAcerto|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaAcerto|Equal0~1_combout\ = ( \passeData|registradorMemoria|B\(1) & ( \passeData|a4|Mux1~0_combout\ & ( !\SW[6]~input_o\ $ (((!\passeData|a3|Mux1~0_combout\ & !\passeData|registradorMemoria|B\(0)))) ) ) ) # ( 
-- !\passeData|registradorMemoria|B\(1) & ( \passeData|a4|Mux1~0_combout\ & ( !\SW[6]~input_o\ $ (((!\passeData|a2|Mux1~0_combout\) # (!\passeData|registradorMemoria|B\(0)))) ) ) ) # ( \passeData|registradorMemoria|B\(1) & ( !\passeData|a4|Mux1~0_combout\ & 
-- ( !\SW[6]~input_o\ $ (((!\passeData|a3|Mux1~0_combout\) # (\passeData|registradorMemoria|B\(0)))) ) ) ) # ( !\passeData|registradorMemoria|B\(1) & ( !\passeData|a4|Mux1~0_combout\ & ( !\SW[6]~input_o\ $ (((!\passeData|a2|Mux1~0_combout\) # 
-- (!\passeData|registradorMemoria|B\(0)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101011010001111000000111100001111010110100011110011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|a2|ALT_INV_Mux1~0_combout\,
	datab => \passeData|a3|ALT_INV_Mux1~0_combout\,
	datac => \ALT_INV_SW[6]~input_o\,
	datad => \passeData|registradorMemoria|ALT_INV_B\(0),
	datae => \passeData|registradorMemoria|ALT_INV_B\(1),
	dataf => \passeData|a4|ALT_INV_Mux1~0_combout\,
	combout => \passeData|ChecaAcerto|Equal0~1_combout\);

-- Location: MLABCELL_X82_Y10_N12
\passeData|encontraResposta|Op[5]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|encontraResposta|Op[5]~8_combout\ = ( !\passeData|registradorMemoria|B\(0) & ( (!\passeData|contaIteracoes|Iteration\(3) & (\passeData|registradorMemoria|B\(1) & (!\passeData|contaIteracoes|Iteration\(0) $ 
-- (!\passeData|contaIteracoes|Iteration\(1))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000101000000000000010100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|registradorMemoria|ALT_INV_B\(1),
	dataf => \passeData|registradorMemoria|ALT_INV_B\(0),
	combout => \passeData|encontraResposta|Op[5]~8_combout\);

-- Location: IOIBUF_X16_Y0_N18
\SW[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: MLABCELL_X82_Y10_N6
\passeData|ChecaAcerto|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaAcerto|Equal0~0_combout\ = ( \passeData|a4|Mux2~0_combout\ & ( \passeData|a2|Mux2~0_combout\ & ( !\SW[5]~input_o\ $ (((!\passeData|encontraResposta|Op[5]~8_combout\ & !\passeData|registradorMemoria|B\(0)))) ) ) ) # ( 
-- !\passeData|a4|Mux2~0_combout\ & ( \passeData|a2|Mux2~0_combout\ & ( !\SW[5]~input_o\ $ (((!\passeData|encontraResposta|Op[5]~8_combout\ & ((!\passeData|registradorMemoria|B\(0)) # (\passeData|registradorMemoria|B\(1)))))) ) ) ) # ( 
-- \passeData|a4|Mux2~0_combout\ & ( !\passeData|a2|Mux2~0_combout\ & ( !\SW[5]~input_o\ $ (((!\passeData|encontraResposta|Op[5]~8_combout\ & ((!\passeData|registradorMemoria|B\(1)) # (!\passeData|registradorMemoria|B\(0)))))) ) ) ) # ( 
-- !\passeData|a4|Mux2~0_combout\ & ( !\passeData|a2|Mux2~0_combout\ & ( !\passeData|encontraResposta|Op[5]~8_combout\ $ (!\SW[5]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011110000111100001111000111100000111100101101000011110011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|registradorMemoria|ALT_INV_B\(1),
	datab => \passeData|encontraResposta|ALT_INV_Op[5]~8_combout\,
	datac => \ALT_INV_SW[5]~input_o\,
	datad => \passeData|registradorMemoria|ALT_INV_B\(0),
	datae => \passeData|a4|ALT_INV_Mux2~0_combout\,
	dataf => \passeData|a2|ALT_INV_Mux2~0_combout\,
	combout => \passeData|ChecaAcerto|Equal0~0_combout\);

-- Location: LABCELL_X83_Y12_N21
\passeData|resultadoUltimo|Equal2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|Equal2~0_combout\ = ( !\passeData|registradorNivel|B\(1) & ( \passeData|registradorNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \passeData|registradorNivel|ALT_INV_B\(1),
	dataf => \passeData|registradorNivel|ALT_INV_B\(0),
	combout => \passeData|resultadoUltimo|Equal2~0_combout\);

-- Location: IOIBUF_X8_Y0_N35
\SW[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: MLABCELL_X82_Y10_N42
\passeData|ChecaAcerto|Equal0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaAcerto|Equal0~4_combout\ = ( \passeData|encontraResposta|Op[1]~0_combout\ & ( \passeData|encontraResposta|Op[0]~2_combout\ & ( (\SW[0]~input_o\ & (\SW[1]~input_o\ & (!\passeData|encontraResposta|Op[2]~3_combout\ $ (\SW[2]~input_o\)))) ) ) 
-- ) # ( !\passeData|encontraResposta|Op[1]~0_combout\ & ( \passeData|encontraResposta|Op[0]~2_combout\ & ( (\SW[0]~input_o\ & (!\SW[1]~input_o\ & (!\passeData|encontraResposta|Op[2]~3_combout\ $ (\SW[2]~input_o\)))) ) ) ) # ( 
-- \passeData|encontraResposta|Op[1]~0_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( (!\SW[0]~input_o\ & (\SW[1]~input_o\ & (!\passeData|encontraResposta|Op[2]~3_combout\ $ (\SW[2]~input_o\)))) ) ) ) # ( 
-- !\passeData|encontraResposta|Op[1]~0_combout\ & ( !\passeData|encontraResposta|Op[0]~2_combout\ & ( (!\SW[0]~input_o\ & (!\SW[1]~input_o\ & (!\passeData|encontraResposta|Op[2]~3_combout\ $ (\SW[2]~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000001000000000000000001000001001000001000000000000000001000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[0]~input_o\,
	datab => \passeData|encontraResposta|ALT_INV_Op[2]~3_combout\,
	datac => \ALT_INV_SW[2]~input_o\,
	datad => \ALT_INV_SW[1]~input_o\,
	datae => \passeData|encontraResposta|ALT_INV_Op[1]~0_combout\,
	dataf => \passeData|encontraResposta|ALT_INV_Op[0]~2_combout\,
	combout => \passeData|ChecaAcerto|Equal0~4_combout\);

-- Location: MLABCELL_X82_Y10_N0
\passeData|resultadoUltimo|pontuacao[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|pontuacao[1]~2_combout\ = ( \passeData|resultadoUltimo|Equal2~0_combout\ & ( \passeData|ChecaAcerto|Equal0~4_combout\ & ( (\passeData|ChecaAcerto|Equal0~3_combout\ & (!\passeData|ChecaAcerto|Equal0~2_combout\ & 
-- (!\passeData|ChecaAcerto|Equal0~1_combout\ & !\passeData|ChecaAcerto|Equal0~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\,
	datab => \passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\,
	datac => \passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\,
	datad => \passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\,
	datae => \passeData|resultadoUltimo|ALT_INV_Equal2~0_combout\,
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\,
	combout => \passeData|resultadoUltimo|pontuacao[1]~2_combout\);

-- Location: FF_X82_Y10_N2
\passeData|mostraResultado|B[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|resultadoUltimo|pontuacao[1]~2_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraResultado|B\(1));

-- Location: MLABCELL_X82_Y10_N3
\passeData|resultadoUltimo|pontuacao[2]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|pontuacao[2]~3_combout\ = ( \passeData|resultadoUltimo|Equal2~1_combout\ & ( \passeData|ChecaAcerto|Equal0~4_combout\ & ( (\passeData|ChecaAcerto|Equal0~3_combout\ & (!\passeData|ChecaAcerto|Equal0~2_combout\ & 
-- (!\passeData|ChecaAcerto|Equal0~0_combout\ & !\passeData|ChecaAcerto|Equal0~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\,
	datab => \passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\,
	datac => \passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\,
	datad => \passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\,
	datae => \passeData|resultadoUltimo|ALT_INV_Equal2~1_combout\,
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\,
	combout => \passeData|resultadoUltimo|pontuacao[2]~3_combout\);

-- Location: FF_X82_Y10_N5
\passeData|mostraResultado|B[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|resultadoUltimo|pontuacao[2]~3_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraResultado|B\(2));

-- Location: LABCELL_X83_Y12_N0
\passeData|resultadoUltimo|pontuacao[3]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|pontuacao[3]~0_combout\ = ( \passeData|registradorNivel|B\(1) & ( \passeData|registradorNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \passeData|registradorNivel|ALT_INV_B\(1),
	dataf => \passeData|registradorNivel|ALT_INV_B\(0),
	combout => \passeData|resultadoUltimo|pontuacao[3]~0_combout\);

-- Location: MLABCELL_X82_Y10_N51
\passeData|resultadoUltimo|pontuacao[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|pontuacao[3]~1_combout\ = ( \passeData|resultadoUltimo|pontuacao[3]~0_combout\ & ( \passeData|ChecaAcerto|Equal0~4_combout\ & ( (\passeData|ChecaAcerto|Equal0~3_combout\ & (!\passeData|ChecaAcerto|Equal0~2_combout\ & 
-- (!\passeData|ChecaAcerto|Equal0~0_combout\ & !\passeData|ChecaAcerto|Equal0~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\,
	datab => \passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\,
	datac => \passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\,
	datad => \passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\,
	datae => \passeData|resultadoUltimo|ALT_INV_pontuacao[3]~0_combout\,
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\,
	combout => \passeData|resultadoUltimo|pontuacao[3]~1_combout\);

-- Location: FF_X82_Y10_N53
\passeData|mostraResultado|B[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|resultadoUltimo|pontuacao[3]~1_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraResultado|B\(3));

-- Location: MLABCELL_X82_Y10_N48
\passeData|resultadoUltimo|pontuacao[0]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|resultadoUltimo|pontuacao[0]~4_combout\ = ( \passeData|resultadoUltimo|Equal2~2_combout\ & ( \passeData|ChecaAcerto|Equal0~4_combout\ & ( (\passeData|ChecaAcerto|Equal0~3_combout\ & (!\passeData|ChecaAcerto|Equal0~2_combout\ & 
-- (!\passeData|ChecaAcerto|Equal0~1_combout\ & !\passeData|ChecaAcerto|Equal0~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\,
	datab => \passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\,
	datac => \passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\,
	datad => \passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\,
	datae => \passeData|resultadoUltimo|ALT_INV_Equal2~2_combout\,
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\,
	combout => \passeData|resultadoUltimo|pontuacao[0]~4_combout\);

-- Location: FF_X82_Y10_N50
\passeData|mostraResultado|B[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|resultadoUltimo|pontuacao[0]~4_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|mostraResultado|B\(0));

-- Location: LABCELL_X83_Y11_N12
\passeData|pontuacaoCasaZero|F[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|pontuacaoCasaZero|F[0]~0_combout\ = ( \passeData|mostraResultado|B\(3) & ( \passeData|mostraResultado|B\(0) & ( !\passeData|mostraResultado|B\(1) $ (!\passeData|mostraResultado|B\(2)) ) ) ) # ( !\passeData|mostraResultado|B\(3) & ( 
-- \passeData|mostraResultado|B\(0) & ( (!\passeData|mostraResultado|B\(1) & !\passeData|mostraResultado|B\(2)) ) ) ) # ( !\passeData|mostraResultado|B\(3) & ( !\passeData|mostraResultado|B\(0) & ( (!\passeData|mostraResultado|B\(1) & 
-- \passeData|mostraResultado|B\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000000000000000011000000110000000011110000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|mostraResultado|ALT_INV_B\(1),
	datac => \passeData|mostraResultado|ALT_INV_B\(2),
	datae => \passeData|mostraResultado|ALT_INV_B\(3),
	dataf => \passeData|mostraResultado|ALT_INV_B\(0),
	combout => \passeData|pontuacaoCasaZero|F[0]~0_combout\);

-- Location: MLABCELL_X84_Y13_N24
\passeData|pontuacaoCasaZero|F[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|pontuacaoCasaZero|F[1]~1_combout\ = ( \passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(1) & ( \passeData|mostraResultado|B\(3) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(1) & ( 
-- \passeData|mostraResultado|B\(2) ) ) ) # ( \passeData|mostraResultado|B\(0) & ( !\passeData|mostraResultado|B\(1) & ( (!\passeData|mostraResultado|B\(3) & \passeData|mostraResultado|B\(2)) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( 
-- !\passeData|mostraResultado|B\(1) & ( (\passeData|mostraResultado|B\(3) & \passeData|mostraResultado|B\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000011000000110000001111000011110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|mostraResultado|ALT_INV_B\(3),
	datac => \passeData|mostraResultado|ALT_INV_B\(2),
	datae => \passeData|mostraResultado|ALT_INV_B\(0),
	dataf => \passeData|mostraResultado|ALT_INV_B\(1),
	combout => \passeData|pontuacaoCasaZero|F[1]~1_combout\);

-- Location: MLABCELL_X84_Y13_N36
\passeData|pontuacaoCasaZero|F[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|pontuacaoCasaZero|F[2]~2_combout\ = ( \passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(1) & ( (\passeData|mostraResultado|B\(3) & \passeData|mostraResultado|B\(2)) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( 
-- \passeData|mostraResultado|B\(1) & ( !\passeData|mostraResultado|B\(3) $ (\passeData|mostraResultado|B\(2)) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( !\passeData|mostraResultado|B\(1) & ( (\passeData|mostraResultado|B\(3) & 
-- \passeData|mostraResultado|B\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000000000000011000011110000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|mostraResultado|ALT_INV_B\(3),
	datac => \passeData|mostraResultado|ALT_INV_B\(2),
	datae => \passeData|mostraResultado|ALT_INV_B\(0),
	dataf => \passeData|mostraResultado|ALT_INV_B\(1),
	combout => \passeData|pontuacaoCasaZero|F[2]~2_combout\);

-- Location: MLABCELL_X84_Y13_N45
\passeData|pontuacaoCasaZero|F[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|pontuacaoCasaZero|F[3]~3_combout\ = ( \passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(2) & ( \passeData|mostraResultado|B\(1) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(2) & ( 
-- (!\passeData|mostraResultado|B\(3) & !\passeData|mostraResultado|B\(1)) ) ) ) # ( \passeData|mostraResultado|B\(0) & ( !\passeData|mostraResultado|B\(2) & ( (!\passeData|mostraResultado|B\(3) & !\passeData|mostraResultado|B\(1)) ) ) ) # ( 
-- !\passeData|mostraResultado|B\(0) & ( !\passeData|mostraResultado|B\(2) & ( (\passeData|mostraResultado|B\(3) & \passeData|mostraResultado|B\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011110000001100000011000000110000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|mostraResultado|ALT_INV_B\(3),
	datac => \passeData|mostraResultado|ALT_INV_B\(1),
	datae => \passeData|mostraResultado|ALT_INV_B\(0),
	dataf => \passeData|mostraResultado|ALT_INV_B\(2),
	combout => \passeData|pontuacaoCasaZero|F[3]~3_combout\);

-- Location: MLABCELL_X84_Y13_N12
\passeData|pontuacaoCasaZero|F[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|pontuacaoCasaZero|F[4]~4_combout\ = ( \passeData|mostraResultado|B\(0) & ( (!\passeData|mostraResultado|B\(3)) # ((!\passeData|mostraResultado|B\(1) & !\passeData|mostraResultado|B\(2))) ) ) # ( !\passeData|mostraResultado|B\(0) & ( 
-- (!\passeData|mostraResultado|B\(1) & (\passeData|mostraResultado|B\(2) & !\passeData|mostraResultado|B\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000111111111100000000001100000000001111111111000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|mostraResultado|ALT_INV_B\(1),
	datac => \passeData|mostraResultado|ALT_INV_B\(2),
	datad => \passeData|mostraResultado|ALT_INV_B\(3),
	datae => \passeData|mostraResultado|ALT_INV_B\(0),
	combout => \passeData|pontuacaoCasaZero|F[4]~4_combout\);

-- Location: MLABCELL_X84_Y13_N18
\passeData|pontuacaoCasaZero|F[5]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|pontuacaoCasaZero|F[5]~5_combout\ = ( \passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(1) & ( !\passeData|mostraResultado|B\(3) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(1) & ( 
-- (!\passeData|mostraResultado|B\(3) & !\passeData|mostraResultado|B\(2)) ) ) ) # ( \passeData|mostraResultado|B\(0) & ( !\passeData|mostraResultado|B\(1) & ( !\passeData|mostraResultado|B\(3) $ (\passeData|mostraResultado|B\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110000111100001111000000110000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|mostraResultado|ALT_INV_B\(3),
	datac => \passeData|mostraResultado|ALT_INV_B\(2),
	datae => \passeData|mostraResultado|ALT_INV_B\(0),
	dataf => \passeData|mostraResultado|ALT_INV_B\(1),
	combout => \passeData|pontuacaoCasaZero|F[5]~5_combout\);

-- Location: MLABCELL_X84_Y13_N0
\passeData|pontuacaoCasaZero|F[6]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|pontuacaoCasaZero|F[6]~6_combout\ = ( \passeData|mostraResultado|B\(0) & ( \passeData|mostraResultado|B\(1) & ( (!\passeData|mostraResultado|B\(2)) # (\passeData|mostraResultado|B\(3)) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( 
-- \passeData|mostraResultado|B\(1) ) ) # ( \passeData|mostraResultado|B\(0) & ( !\passeData|mostraResultado|B\(1) & ( (\passeData|mostraResultado|B\(2)) # (\passeData|mostraResultado|B\(3)) ) ) ) # ( !\passeData|mostraResultado|B\(0) & ( 
-- !\passeData|mostraResultado|B\(1) & ( !\passeData|mostraResultado|B\(3) $ (!\passeData|mostraResultado|B\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011110000111100001111110011111111111111111111111111001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|mostraResultado|ALT_INV_B\(3),
	datac => \passeData|mostraResultado|ALT_INV_B\(2),
	datae => \passeData|mostraResultado|ALT_INV_B\(0),
	dataf => \passeData|mostraResultado|ALT_INV_B\(1),
	combout => \passeData|pontuacaoCasaZero|F[6]~6_combout\);

-- Location: FF_X84_Y12_N17
\passeData|regNivel|B[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|registradorNivel|B\(1),
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|regNivel|B\(1));

-- Location: FF_X84_Y12_N49
\passeData|regNivel|B[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \passeData|registradorNivel|B\(0),
	clrn => \control|EA.E0~q\,
	sload => VCC,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|regNivel|B\(0));

-- Location: LABCELL_X83_Y12_N27
\passeData|CasaNivel|Equal13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|CasaNivel|Equal13~0_combout\ = ( !\passeData|regNivel|B\(1) & ( \passeData|regNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \passeData|regNivel|ALT_INV_B\(1),
	dataf => \passeData|regNivel|ALT_INV_B\(0),
	combout => \passeData|CasaNivel|Equal13~0_combout\);

-- Location: LABCELL_X83_Y12_N9
\passeData|CasaNivel|F[2]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|CasaNivel|F[2]~0_combout\ = ( \passeData|regNivel|B\(1) & ( \passeData|regNivel|B\(0) ) ) # ( !\passeData|regNivel|B\(1) & ( \passeData|regNivel|B\(0) ) ) # ( !\passeData|regNivel|B\(1) & ( !\passeData|regNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \passeData|regNivel|ALT_INV_B\(1),
	dataf => \passeData|regNivel|ALT_INV_B\(0),
	combout => \passeData|CasaNivel|F[2]~0_combout\);

-- Location: MLABCELL_X87_Y15_N39
\passeData|CasaNivel|F[5]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|CasaNivel|F[5]~1_combout\ = ( \passeData|regNivel|B\(1) ) # ( !\passeData|regNivel|B\(1) & ( \passeData|regNivel|B\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101111111111111111101010101010101011111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|regNivel|ALT_INV_B\(0),
	datae => \passeData|regNivel|ALT_INV_B\(1),
	combout => \passeData|CasaNivel|F[5]~1_combout\);

-- Location: MLABCELL_X84_Y11_N36
\passeData|regL|B[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|regL|B[0]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \passeData|regL|B[0]~feeder_combout\);

-- Location: FF_X84_Y11_N37
\passeData|regL|B[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|regL|B[0]~feeder_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|regL|B\(0));

-- Location: MLABCELL_X84_Y11_N9
\passeData|regL|B[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|regL|B[1]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \passeData|regL|B[1]~feeder_combout\);

-- Location: FF_X84_Y11_N10
\passeData|regL|B[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|regL|B[1]~feeder_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|regL|B\(1));

-- Location: MLABCELL_X84_Y11_N15
\passeData|regL|B[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|regL|B[2]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \passeData|regL|B[2]~feeder_combout\);

-- Location: FF_X84_Y11_N16
\passeData|regL|B[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|regL|B[2]~feeder_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|regL|B\(2));

-- Location: MLABCELL_X84_Y11_N42
\passeData|regL|B[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|regL|B[6]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \passeData|regL|B[6]~feeder_combout\);

-- Location: FF_X84_Y11_N43
\passeData|regL|B[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK_50~inputCLKENA0_outclk\,
	d => \passeData|regL|B[6]~feeder_combout\,
	clrn => \control|EA.E0~q\,
	ena => \control|EA.E1~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \passeData|regL|B\(6));

-- Location: LABCELL_X83_Y10_N36
\passeData|Mux19~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux19~0_combout\ = ( \passeData|contaIteracoes|Iteration\(3) & ( (!\passeData|contaIteracoes|Iteration\(2) & ((!\passeData|contaIteracoes|Iteration\(1)) # (!\passeData|contaIteracoes|Iteration\(0)))) ) ) # ( 
-- !\passeData|contaIteracoes|Iteration\(3) & ( (!\passeData|contaIteracoes|Iteration\(1) & ((\passeData|contaIteracoes|Iteration\(0)) # (\passeData|contaIteracoes|Iteration\(2)))) # (\passeData|contaIteracoes|Iteration\(1) & 
-- ((!\passeData|contaIteracoes|Iteration\(2)) # (!\passeData|contaIteracoes|Iteration\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111111111100001111111111110011110000110000001111000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|Mux19~0_combout\);

-- Location: MLABCELL_X82_Y10_N30
\passeData|ChecaAcerto|Equal0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|ChecaAcerto|Equal0~5_combout\ = ( \passeData|ChecaAcerto|Equal0~4_combout\ & ( (!\passeData|ChecaAcerto|Equal0~1_combout\ & (\passeData|ChecaAcerto|Equal0~3_combout\ & (!\passeData|ChecaAcerto|Equal0~2_combout\ & 
-- !\passeData|ChecaAcerto|Equal0~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000100000000000000010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\,
	datab => \passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\,
	datac => \passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\,
	datad => \passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\,
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\,
	combout => \passeData|ChecaAcerto|Equal0~5_combout\);

-- Location: MLABCELL_X82_Y10_N18
\passeData|Mux18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux18~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (!\passeData|contaIteracoes|Iteration\(3) & (!\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(1) & !\passeData|contaIteracoes|Iteration\(0)))) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux18~0_combout\);

-- Location: MLABCELL_X82_Y10_N21
\passeData|Leds0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds0~combout\ = ( \passeData|Mux18~0_combout\ & ( (!\passeData|Mux19~0_combout\) # (\passeData|Leds0~combout\) ) ) # ( !\passeData|Mux18~0_combout\ & ( (\passeData|Leds0~combout\ & \passeData|Mux19~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111111111111000011111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Leds0~combout\,
	datad => \passeData|ALT_INV_Mux19~0_combout\,
	dataf => \passeData|ALT_INV_Mux18~0_combout\,
	combout => \passeData|Leds0~combout\);

-- Location: LABCELL_X81_Y10_N42
\passeData|Mux16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux16~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(1) & (!\passeData|contaIteracoes|Iteration\(2) & !\passeData|contaIteracoes|Iteration\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux16~0_combout\);

-- Location: LABCELL_X81_Y10_N33
\passeData|Mux17~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux17~0_combout\ = ( \passeData|contaIteracoes|Iteration\(1) & ( (!\passeData|contaIteracoes|Iteration\(0) & (\passeData|contaIteracoes|Iteration\(2) & \passeData|contaIteracoes|Iteration\(3))) # (\passeData|contaIteracoes|Iteration\(0) & 
-- ((\passeData|contaIteracoes|Iteration\(3)) # (\passeData|contaIteracoes|Iteration\(2)))) ) ) # ( !\passeData|contaIteracoes|Iteration\(1) & ( (!\passeData|contaIteracoes|Iteration\(2) & (\passeData|contaIteracoes|Iteration\(0) & 
-- !\passeData|contaIteracoes|Iteration\(3))) # (\passeData|contaIteracoes|Iteration\(2) & ((\passeData|contaIteracoes|Iteration\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000001111010100000000111100000101010111110000010101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	combout => \passeData|Mux17~0_combout\);

-- Location: LABCELL_X81_Y10_N45
\passeData|Leds1\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds1~combout\ = ( \passeData|Mux17~0_combout\ & ( \passeData|Mux16~0_combout\ ) ) # ( !\passeData|Mux17~0_combout\ & ( \passeData|Leds1~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Leds1~combout\,
	datad => \passeData|ALT_INV_Mux16~0_combout\,
	dataf => \passeData|ALT_INV_Mux17~0_combout\,
	combout => \passeData|Leds1~combout\);

-- Location: LABCELL_X81_Y10_N3
\passeData|Mux15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux15~0_combout\ = ( \passeData|contaIteracoes|Iteration\(1) & ( (!\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(2) $ (\passeData|contaIteracoes|Iteration\(3)))) # (\passeData|contaIteracoes|Iteration\(0) & 
-- ((\passeData|contaIteracoes|Iteration\(3)) # (\passeData|contaIteracoes|Iteration\(2)))) ) ) # ( !\passeData|contaIteracoes|Iteration\(1) & ( (\passeData|contaIteracoes|Iteration\(2) & \passeData|contaIteracoes|Iteration\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111110100101010111111010010101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	combout => \passeData|Mux15~0_combout\);

-- Location: LABCELL_X81_Y10_N51
\passeData|Mux14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux14~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (!\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(0) & (\passeData|contaIteracoes|Iteration\(1) & !\passeData|contaIteracoes|Iteration\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000000000000000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux14~0_combout\);

-- Location: LABCELL_X81_Y10_N0
\passeData|Leds2\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds2~combout\ = ( \passeData|Mux14~0_combout\ & ( (\passeData|Mux15~0_combout\) # (\passeData|Leds2~combout\) ) ) # ( !\passeData|Mux14~0_combout\ & ( (\passeData|Leds2~combout\ & !\passeData|Mux15~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Leds2~combout\,
	datad => \passeData|ALT_INV_Mux15~0_combout\,
	dataf => \passeData|ALT_INV_Mux14~0_combout\,
	combout => \passeData|Leds2~combout\);

-- Location: LABCELL_X83_Y10_N33
\passeData|Mux12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux12~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (!\passeData|contaIteracoes|Iteration\(3) & (\passeData|contaIteracoes|Iteration\(0) & (\passeData|contaIteracoes|Iteration\(1) & !\passeData|contaIteracoes|Iteration\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000010000000000000001000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux12~0_combout\);

-- Location: MLABCELL_X84_Y10_N42
\passeData|Mux13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux13~0_combout\ = ( \passeData|contaIteracoes|Iteration\(0) & ( \passeData|contaIteracoes|Iteration\(3) & ( (\passeData|contaIteracoes|Iteration\(1)) # (\passeData|contaIteracoes|Iteration\(2)) ) ) ) # ( 
-- !\passeData|contaIteracoes|Iteration\(0) & ( \passeData|contaIteracoes|Iteration\(3) & ( \passeData|contaIteracoes|Iteration\(2) ) ) ) # ( \passeData|contaIteracoes|Iteration\(0) & ( !\passeData|contaIteracoes|Iteration\(3) & ( 
-- \passeData|contaIteracoes|Iteration\(1) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100001111000011110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datae => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|Mux13~0_combout\);

-- Location: MLABCELL_X84_Y10_N57
\passeData|Leds3\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds3~combout\ = ( \passeData|Mux13~0_combout\ & ( \passeData|Mux12~0_combout\ ) ) # ( !\passeData|Mux13~0_combout\ & ( \passeData|Leds3~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Leds3~combout\,
	datad => \passeData|ALT_INV_Mux12~0_combout\,
	dataf => \passeData|ALT_INV_Mux13~0_combout\,
	combout => \passeData|Leds3~combout\);

-- Location: LABCELL_X81_Y10_N36
\passeData|Mux11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux11~0_combout\ = ( \passeData|contaIteracoes|Iteration\(2) & ( (!\passeData|contaIteracoes|Iteration\(1) $ (\passeData|contaIteracoes|Iteration\(0))) # (\passeData|contaIteracoes|Iteration\(3)) ) ) # ( !\passeData|contaIteracoes|Iteration\(2) 
-- & ( (\passeData|contaIteracoes|Iteration\(1) & (\passeData|contaIteracoes|Iteration\(0) & \passeData|contaIteracoes|Iteration\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000000001111000011111111111100001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	combout => \passeData|Mux11~0_combout\);

-- Location: LABCELL_X81_Y10_N6
\passeData|Mux10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux10~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (!\passeData|contaIteracoes|Iteration\(0) & (!\passeData|contaIteracoes|Iteration\(1) & (\passeData|contaIteracoes|Iteration\(2) & !\passeData|contaIteracoes|Iteration\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000000000000000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux10~0_combout\);

-- Location: LABCELL_X81_Y10_N9
\passeData|Leds4\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds4~combout\ = ( \passeData|Mux10~0_combout\ & ( (\passeData|Mux11~0_combout\) # (\passeData|Leds4~combout\) ) ) # ( !\passeData|Mux10~0_combout\ & ( (\passeData|Leds4~combout\ & !\passeData|Mux11~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Leds4~combout\,
	datad => \passeData|ALT_INV_Mux11~0_combout\,
	dataf => \passeData|ALT_INV_Mux10~0_combout\,
	combout => \passeData|Leds4~combout\);

-- Location: LABCELL_X81_Y10_N27
\passeData|Mux9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux9~0_combout\ = ( \passeData|contaIteracoes|Iteration\(1) & ( (!\passeData|contaIteracoes|Iteration\(0) & (\passeData|contaIteracoes|Iteration\(2) & \passeData|contaIteracoes|Iteration\(3))) # (\passeData|contaIteracoes|Iteration\(0) & 
-- ((\passeData|contaIteracoes|Iteration\(3)) # (\passeData|contaIteracoes|Iteration\(2)))) ) ) # ( !\passeData|contaIteracoes|Iteration\(1) & ( (\passeData|contaIteracoes|Iteration\(2) & ((\passeData|contaIteracoes|Iteration\(3)) # 
-- (\passeData|contaIteracoes|Iteration\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100001111000001010000111100000101010111110000010101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	combout => \passeData|Mux9~0_combout\);

-- Location: LABCELL_X81_Y10_N39
\passeData|Mux8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux8~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(1) & !\passeData|contaIteracoes|Iteration\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001000100000000000100010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux8~0_combout\);

-- Location: LABCELL_X81_Y10_N54
\passeData|Leds5\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds5~combout\ = ( \passeData|Mux8~0_combout\ & ( (\passeData|Mux9~0_combout\) # (\passeData|Leds5~combout\) ) ) # ( !\passeData|Mux8~0_combout\ & ( (\passeData|Leds5~combout\ & !\passeData|Mux9~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Leds5~combout\,
	datad => \passeData|ALT_INV_Mux9~0_combout\,
	dataf => \passeData|ALT_INV_Mux8~0_combout\,
	combout => \passeData|Leds5~combout\);

-- Location: MLABCELL_X82_Y10_N15
\passeData|Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux6~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (!\passeData|contaIteracoes|Iteration\(3) & (!\passeData|contaIteracoes|Iteration\(0) & (\passeData|contaIteracoes|Iteration\(2) & \passeData|contaIteracoes|Iteration\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010000000000000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux6~0_combout\);

-- Location: MLABCELL_X84_Y10_N21
\passeData|Mux7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux7~0_combout\ = ( \passeData|contaIteracoes|Iteration\(3) & ( ((\passeData|contaIteracoes|Iteration\(1) & \passeData|contaIteracoes|Iteration\(0))) # (\passeData|contaIteracoes|Iteration\(2)) ) ) # ( !\passeData|contaIteracoes|Iteration\(3) & 
-- ( (\passeData|contaIteracoes|Iteration\(2) & \passeData|contaIteracoes|Iteration\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010101010101010111110101010101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|Mux7~0_combout\);

-- Location: MLABCELL_X82_Y10_N33
\passeData|Leds6\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds6~combout\ = ( \passeData|Mux7~0_combout\ & ( \passeData|Mux6~0_combout\ ) ) # ( !\passeData|Mux7~0_combout\ & ( \passeData|Leds6~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Mux6~0_combout\,
	datad => \passeData|ALT_INV_Leds6~combout\,
	dataf => \passeData|ALT_INV_Mux7~0_combout\,
	combout => \passeData|Leds6~combout\);

-- Location: MLABCELL_X84_Y10_N0
\passeData|Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux5~0_combout\ = ( \passeData|contaIteracoes|Iteration\(2) & ( ((\passeData|contaIteracoes|Iteration\(1) & \passeData|contaIteracoes|Iteration\(0))) # (\passeData|contaIteracoes|Iteration\(3)) ) ) # ( !\passeData|contaIteracoes|Iteration\(2) & 
-- ( (\passeData|contaIteracoes|Iteration\(3) & (!\passeData|contaIteracoes|Iteration\(1) $ (\passeData|contaIteracoes|Iteration\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000011000000001100001100000011111111110000001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	combout => \passeData|Mux5~0_combout\);

-- Location: LABCELL_X83_Y10_N30
\passeData|Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux4~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (!\passeData|contaIteracoes|Iteration\(0) & \passeData|compDez|Equal0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datad => \passeData|compDez|ALT_INV_Equal0~0_combout\,
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux4~0_combout\);

-- Location: LABCELL_X83_Y10_N42
\passeData|Leds7\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds7~combout\ = ( \passeData|Mux4~0_combout\ & ( (\passeData|Mux5~0_combout\) # (\passeData|Leds7~combout\) ) ) # ( !\passeData|Mux4~0_combout\ & ( (\passeData|Leds7~combout\ & !\passeData|Mux5~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100110000000000110011111111110011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ALT_INV_Leds7~combout\,
	datad => \passeData|ALT_INV_Mux5~0_combout\,
	dataf => \passeData|ALT_INV_Mux4~0_combout\,
	combout => \passeData|Leds7~combout\);

-- Location: LABCELL_X83_Y10_N12
\passeData|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux2~0_combout\ = ( \passeData|ChecaAcerto|Equal0~3_combout\ & ( !\passeData|ChecaAcerto|Equal0~2_combout\ & ( (\passeData|compDez|Equal0~0_combout\ & (!\passeData|ChecaAcerto|Equal0~1_combout\ & (!\passeData|ChecaAcerto|Equal0~0_combout\ & 
-- \passeData|ChecaAcerto|Equal0~4_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000100000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|compDez|ALT_INV_Equal0~0_combout\,
	datab => \passeData|ChecaAcerto|ALT_INV_Equal0~1_combout\,
	datac => \passeData|ChecaAcerto|ALT_INV_Equal0~0_combout\,
	datad => \passeData|ChecaAcerto|ALT_INV_Equal0~4_combout\,
	datae => \passeData|ChecaAcerto|ALT_INV_Equal0~3_combout\,
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~2_combout\,
	combout => \passeData|Mux2~0_combout\);

-- Location: MLABCELL_X84_Y10_N9
\passeData|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux3~0_combout\ = ( \passeData|contaIteracoes|Iteration\(3) & ( (\passeData|contaIteracoes|Iteration\(2)) # (\passeData|contaIteracoes|Iteration\(0)) ) ) # ( !\passeData|contaIteracoes|Iteration\(3) & ( (\passeData|contaIteracoes|Iteration\(0) 
-- & (\passeData|contaIteracoes|Iteration\(1) & \passeData|contaIteracoes|Iteration\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010101010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	combout => \passeData|Mux3~0_combout\);

-- Location: MLABCELL_X84_Y10_N51
\passeData|Leds8\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds8~combout\ = ( \passeData|Mux3~0_combout\ & ( \passeData|Mux2~0_combout\ ) ) # ( !\passeData|Mux3~0_combout\ & ( \passeData|Leds8~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \passeData|ALT_INV_Leds8~combout\,
	datad => \passeData|ALT_INV_Mux2~0_combout\,
	dataf => \passeData|ALT_INV_Mux3~0_combout\,
	combout => \passeData|Leds8~combout\);

-- Location: MLABCELL_X82_Y10_N36
\passeData|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux1~0_combout\ = ( \passeData|contaIteracoes|Iteration\(2) & ( ((\passeData|contaIteracoes|Iteration\(0) & \passeData|contaIteracoes|Iteration\(1))) # (\passeData|contaIteracoes|Iteration\(3)) ) ) # ( !\passeData|contaIteracoes|Iteration\(2) & 
-- ( (\passeData|contaIteracoes|Iteration\(3) & \passeData|contaIteracoes|Iteration\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100001111001111110000111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	dataf => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	combout => \passeData|Mux1~0_combout\);

-- Location: LABCELL_X81_Y10_N15
\passeData|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Mux0~0_combout\ = ( \passeData|ChecaAcerto|Equal0~5_combout\ & ( (!\passeData|contaIteracoes|Iteration\(2) & (!\passeData|contaIteracoes|Iteration\(0) & (\passeData|contaIteracoes|Iteration\(1) & \passeData|contaIteracoes|Iteration\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010000000000000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \passeData|contaIteracoes|ALT_INV_Iteration\(2),
	datab => \passeData|contaIteracoes|ALT_INV_Iteration\(0),
	datac => \passeData|contaIteracoes|ALT_INV_Iteration\(1),
	datad => \passeData|contaIteracoes|ALT_INV_Iteration\(3),
	dataf => \passeData|ChecaAcerto|ALT_INV_Equal0~5_combout\,
	combout => \passeData|Mux0~0_combout\);

-- Location: LABCELL_X81_Y10_N30
\passeData|Leds9\ : cyclonev_lcell_comb
-- Equation(s):
-- \passeData|Leds9~combout\ = ( \passeData|Mux0~0_combout\ & ( (\passeData|Mux1~0_combout\) # (\passeData|Leds9~combout\) ) ) # ( !\passeData|Mux0~0_combout\ & ( (\passeData|Leds9~combout\ & !\passeData|Mux1~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100110000000000110011111111110011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \passeData|ALT_INV_Leds9~combout\,
	datad => \passeData|ALT_INV_Mux1~0_combout\,
	dataf => \passeData|ALT_INV_Mux0~0_combout\,
	combout => \passeData|Leds9~combout\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: MLABCELL_X39_Y27_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


