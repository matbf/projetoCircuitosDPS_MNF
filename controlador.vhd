library IEEE;
use IEEE.Std_Logic_1164.all;

entity controlador is
	port (
			Reset: in std_logic;
			ContaDez: in std_logic;
			Enter: in std_logic;
			Clk: in std_logic;
			EnableRegGame: out std_logic;
			EnableRegResult: out std_logic;
			EnableRegSetup: out std_logic;
			ResetDatapath: out std_logic
			);			
end controlador;

architecture bhv of controlador is
	type STATES is (E0, E1, E2, E3);
	signal EA, PE: STATES;
	begin	
		P1: process(Reset, Clk)--Estado 0: Init; Estado 1: Setup; Estado 2:Game; Estado 3: Results.
			begin
				if Reset = '0' then
					EA <= E0;

				elsif Clk'event and Clk = '1' then
					EA <= PE;	
				end if;
		end process;
		
		P2: process(EA, ContaDez)
			
			begin
			
				case EA is
					when E0 =>--Init
						ResetDatapath <= '0';
						EnableRegGame <= '0';
						EnableRegResult <= '0';
						EnableRegSetup <= '0';
						if Enter = '0' then
							PE <= E1;
						else
							PE <= E0;
						end if;
					when E1 =>--Setup
						ResetDatapath <= '1';
						EnableRegGame <= '0';
						EnableRegResult <= '0';
						EnableRegSetup <= '1';
						if Enter = '0' then
							PE <= E2;
						else
							PE <= E1;
						end if;
					when E2 =>--Game
						ResetDatapath <= '1';
						EnableRegGame <= '1';
						EnableRegResult <= '0';
						EnableRegSetup <= '0';
						if ContaDez = '1' then
							PE <= E3;
						else 
							PE <= E2;
						end if;
					when E3 =>--Results
						ResetDatapath <= '1';
						EnableRegGame <= '0';
						EnableRegResult <= '1';
						EnableRegSetup <= '0';
						if Enter = '0' then
							PE <= E0;
						else
							PE <= E3;
						end if;
				end case;
		end process;
end bhv;