library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity contadorQuatroBits is

port (
		Clk: in std_logic;
		Reset: in std_logic;
		enable: in std_logic;
		limit: in std_logic_vector(3 downto 0);
		currentIteration: in std_logic_vector(3 downto 0);
		Iteration: out std_logic_vector(3 downto 0)
		);
end contadorQuatroBits;

architecture timer of contadorQuatroBits is
	begin 
	
		process(Clk, reset)
			begin	
				if reset = '0' then
					Iteration <= "0000";
				
				elsif Clk'event and Clk = '1' then
					if enable = '1' then
						Iteration <= currentIteration + '1';
					else 
						Iteration<= currentIteration;
					end if;
				end if;
		end process;
end timer;