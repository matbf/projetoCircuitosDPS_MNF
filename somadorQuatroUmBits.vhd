library IEEE;
use IEEE.Std_Logic_1164.all;
use ieee.std_logic_unsigned.all;

entity somadorQuatroUmBits is 
	port (
			A: in std_logic;
			B: in std_logic_vector(3 downto 0);
			enable: in std_logic;
			Soma: out std_logic_vector(3 downto 0)
			);
end somadorQuatroUmBits;

architecture bhv of somadorQuatroUmBits is
	signal AQuatroBits: std_logic_vector(3 downto 0);
	begin
			AQuatroBits <= "000" & A;
			
			Soma <= (AQuatroBits+B) when enable = '1' else
					  B;
end bhv;