library IEEE;
use IEEE.Std_Logic_1164.all;

entity comparadorOitoBits is
			port(
					comparingConstant: in std_logic_vector(7 downto 0);
					comparingVariable: in std_logic_vector(7 downto 0);
					result: out std_logic
				 );
end comparadorOitoBits;

architecture behaviour of comparadorOitoBits is
	begin 
		result <= '1' when comparingConstant = comparingVariable else
		'0';
end behaviour;		
			