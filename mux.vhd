library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity mux is

port (
		F1: in std_logic_vector(7 downto 0);
		F2: in std_logic_vector(7 downto 0);
		F3: in std_logic_vector(7 downto 0);
		F4: in std_logic_vector(7 downto 0);
		Ctrl: in std_logic_vector(9 downto 8);
		Op: out std_logic_vector(7 downto 0)
		);
end mux;

architecture operacoes of mux is
	begin 
	
	Op <= F1 when Ctrl = "00" else
		   F2 when Ctrl = "01" else
		   F3 when Ctrl = "10" else
		   F4;
	
	end operacoes;

