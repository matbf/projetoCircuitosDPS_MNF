library IEEE;
use IEEE.Std_Logic_1164.all;
use ieee.std_logic_unsigned.all;

--		A maquina consistira de 4 estados. No primeiro, o estado init, a maquina esperara o comando do usuario para iniciar o jogo (KEY(1))
--		No segundo, o estado setup, a maquina esperara o gamer escolher qual nivel de velocidade ele jogara, e qual sequencia de numeros o jogo seguira, 
--ele terminara o input apertando enter (KEY(1))
--		No terceiro estado, o estado game, o jogo esperara o usuario enviar o comando de que inseriu o valor do jogo usando os SW(7 downto 0) com o KEY(1)
--		No quarto estado, o estado results, os resultados da rodada serao computados, e caso seja a decima, o jogo parara. A nota do gamer sera 2 elevado
--ao nivel do jogo, vezes o numero de acertos

entity Datapath is
	port (
			enableSetupReg: in std_logic;
			enableGameReg: in std_logic;
			enableResultReg: in std_logic;
			Switches: in std_logic_vector(9 downto 0);
			Clk: in std_logic;
			Reset: in std_logic;
			numeroConversao: out std_logic_vector(13 downto 0); --Mostra o hexadecimal a ser convertido pelo gamer;
			notaFinal: out std_logic_vector(13 downto 0); --Mostra a nota fim do jogo;
			visorNivel: out std_logic_vector(6 downto 0);
			visorL: out std_logic_vector(6 downto 0);
			contaAteDez: out std_logic;
			Leds: out std_logic_vector(9 downto 0)--Mostra o ponto feito
			);

end Datapath;

architecture game of Datapath is
		signal memoriaEscolhida: std_logic_vector(1 downto 0);
		signal answer, answer1, answer2, answer3, answer4, nextAnswer: std_logic_vector(7 downto 0);
		signal contadorIterations, contadorIterationsOvertime: std_logic_vector(3 downto 0);
		signal contadorTempo, contadorTempoClock: std_logic_vector(31 downto 0);
		signal level, mostraLevel: std_logic_vector(1 downto 0);
		signal levelTimeLimit: std_logic_vector(31 downto 0);
		signal OverTime: std_logic;
		signal point: std_logic;
		signal levelQuatroBits, memoriaQuatroBits: std_logic_vector(3 downto 0);
		signal Points, pontuacao: std_logic_vector(3 downto 0);
		signal calculoFinalPontos, answerFinal, attempt, memoriaFinal: std_logic_vector(7 downto 0);
		signal enFinal, acerto: std_logic;
		signal casaZeroConversao8, casaUmConversao8, pontuacaoFinal, L, mostraL, memoriaOitoBits, casaZeroConversaoReg, casaUmConversaoReg : std_logic_vector(7 downto 0);
		signal casaZeroConversao, casaUmConversao, casaZeroNota, casaUmNota: std_logic_vector(6 downto 0);
		signal Leds0, Leds1, Leds2, Leds3, Leds4, Leds5, Leds6, Leds7, Leds8, Leds9: std_logic;
		
		
		component comparadorOitoBits is
			port(
				  comparingConstant: in std_logic_vector(7 downto 0);
				  comparingVariable: in std_logic_vector(7 downto 0);
				  result: out std_logic
				 );
		end component;
		
		component comparadorQuatroBits is
			port(
				  comparingConstant: in std_logic_vector(3 downto 0);
				  comparingVariable: in std_logic_vector(3 downto 0);
				  result: out std_logic
				 );
		end component;
		
		component comparador32Bits is
			port(
				  comparingConstant: in std_logic_vector(31 downto 0);
				  comparingVariable: in std_logic_vector(31 downto 0);
				  result: out std_logic
				 );
		end component;
		
		component contadorQuatroBits is
			port(
				  Clk: in std_logic;
				  Reset: in std_logic;
				  enable: in std_logic;
				  limit: in std_logic_vector(3 downto 0);
				  currentIteration: in std_logic_vector(3 downto 0);
				  Iteration: out std_logic_vector(3 downto 0)
				  );
		end component;
		
		component contadorOitoBits is
			port(
				  Clk: in std_logic;
				  Reset: in std_logic;
				  enable: in std_logic;
				  limit: in std_logic_vector(7 downto 0);
				  currentIteration: in std_logic_vector(7 downto 0);
				  Iteration: out std_logic_vector(7 downto 0)
				  );
		end component;
		
		component contador32Bits is
			port(
					Clk: in std_logic;
					Reset: in std_logic;
					Enable: in std_logic;
					limit: in std_logic_vector(31 downto 0);
					currentIteration: in std_logic_vector(31 downto 0);
					Iteration: out std_logic_vector(31 downto 0)
				  );
		end component;
		
	
		component somadorQuatroUmBits is 
			port (
					A: in std_logic;
					B: in std_logic_vector(3 downto 0);
					enable: in std_logic;
					Soma: out std_logic_vector(3 downto 0)
					);
		end component;	
		
		component ROM1 is
			port ( 
					address: in std_logic_vector(3 downto 0);
					data: out std_logic_vector(7 downto 0)
					);
		end component;
		
		component ROM2 is
			port ( 
					address: in std_logic_vector(3 downto 0);
					data: out std_logic_vector(7 downto 0)
					);
		end component;
		
		component ROM3 is
			port ( 
					address: in std_logic_vector(3 downto 0);
					data: out std_logic_vector(7 downto 0)
					);
		end component;			
		
		component ROM4 is
			port ( 
					address: in std_logic_vector(3 downto 0);
					data: out std_logic_vector(7 downto 0)
					);
		end component;	
		
		component mux is
			port (
					F1: in std_logic_vector(7 downto 0);
					F2: in std_logic_vector(7 downto 0);
					F3: in std_logic_vector(7 downto 0);
					F4: in std_logic_vector(7 downto 0);
					Ctrl: in std_logic_vector(9 downto 8);
					Op: out std_logic_vector(7 downto 0)
					);
		end component;
		
		component registradorDoisBits is
			port (
					CLK: in std_logic;
					Enable: in std_logic;
					Reset: in std_logic;
					A: in std_logic_vector(1 downto 0);
					B: out std_logic_vector(1 downto 0)
				   );
		end component;
		
		component registradorOitoBits is
			port (
					Clk: in std_logic;
					Enable: in std_logic;
					Reset: in std_logic;
					A: in std_logic_vector(7 downto 0);
					B: out std_logic_vector(7 downto 0)
				   );
		end component;
		
		component decod is
			port (
					A: in std_logic_vector(3 downto 0);
					F: out std_logic_vector(6 downto 0)
				  );
		end component;
		
		component muxPointsFinal is 
			port (
					pontos: in std_logic_vector( 3 downto 0);
					nivel: in std_logic_vector( 1 downto 0);
					pontuacao: out std_logic_vector( 7 downto 0)
					);
		end component;
		
		component mux32 is
			port (
					F1: in std_logic_vector(31 downto 0);
					F2: in std_logic_vector(31 downto 0);
					F3: in std_logic_vector(31 downto 0);
					F4: in std_logic_vector(31 downto 0);
					Ctrl: in std_logic_vector(9 downto 8);
					Op: out std_logic_vector(31 downto 0)
					);
		end component;
		
		begin	
			
			--=============================================ESTADO SETUP================================================================ 
			registradorNivel: registradorDoisBits port map(Clk, enableSetupReg, Reset,  Switches(9 downto 8), level);					
			registradorMemoria: registradorDoisBits port map(Clk, enableSetupReg, Reset, Switches(1 downto 0), memoriaEscolhida);					
			tempo: mux32 port map(x"1DCD64FF", x"0BEBC1FF", x"08F0D17F", x"05F5E0FF", level, levelTimeLimit);
		   --Coloca o nível no seu visor
			regNivel: registradorDoisBits port map(Clk, enableSetupReg, Reset, level, mostraLevel);
			levelQuatroBits <= "00" & mostraLevel;
			CasaNivel: decod port map(levelQuatroBits, visorNivel);
			regL: registradorOitoBits port map(Clk, enableSetupReg, Reset, L, mostraL);
			L <= "01000111";
			visorL <= mostraL(6 downto 0);
			
			--=============================================ESTADO GAME================================================================= 
			--Conta o tempo passado, com teto no limite definido pelo nível
			ClockTempo: contador32Bits port map(Clk, Reset, enableGameReg, levelTimeLimit, contadorTempo, contadorTempoClock);
			contadorTempo <= contadorTempoClock;
			
			--Retorna 1 em OverTime quando o relógio atinge o tempo limite
			ChecaSeLimiteTempo: comparador32Bits port map(contadorTempoClock, levelTimeLimit, OverTime);
			
			--Quando OverTime for atingido, retorna 1 em point se o usuario tiver acertado.
			--respostaOvertime: registradorOitoBits port map(Clk, OverTime, Reset, Switches(7 downto 0), attempt);
			ChecaAcerto: comparadorOitoBits port map(Switches(7 downto 0), answer, point);			
						
			--Toda vez que o tempo limite é atingido (OverTime) soma 1 ao total de pontos, se point for 1.
			pontos: somadorQuatroUmBits port map(acerto, Points, OverTime, pontuacao);

			--Encontra o proximo numero a ser convertido
			a1: ROM1 port map(contadorIterations, answer1);
			a2: ROM2 port map(contadorIterations, answer2);
			a3: ROM3 port map(contadorIterations, answer3);
			a4: ROM4 port map(contadorIterations, answer4);
			encontraResposta: mux port map(answer1, answer2, answer3, answer4, memoriaEscolhida, answer);		
			--Mostra o numero a ser convertido
			--regResposta: registradorOitoBits port map(OverTime, Reset, enableGameReg, answer, answerFinal);
			motraCasaZeroJogo: decod port map(answer(3 downto 0), CasaZeroConversao);
			mostraCasaUmJogo: decod port map(answer(7 downto 4), CasaUmConversao);
			casaZeroConversao8 <= '0' & casaZeroConversao;
			casaUmConversao8 <= '0' & casaUmConversao;
			mostraRegZero: registradorOitoBits port map(Clk, enableGameReg, Reset, casaZeroConversao8, casaZeroConversaoReg);
			mostraRegUm: registradorOitoBits port map(Clk, enableGameReg, Reset, casaUmConversao8, casaUmConversaoReg);
			numeroConversao <= CasaUmConversaoReg(6 downto 0) & CasaZeroConversaoReg(6 downto 0);
			
			--Soma 1 em contadorIteration toda vez que OverTime é atingido
			contaIteracoes: contadorQuatroBits port map(OverTime, Reset, enableGameReg, "1001", contadorIterations, contadorIterationsOverTime);
			contadorIterations <= contadorIterationsOverTime;
			compDez: comparadorQuatroBits port map("1001", contadorIterations, contaAteDez);
			
			
			--Toda vez que OverTime é atingido acende o led equivalente ao contadorIterations se o usuário acertou
			P2: process(contadorIterations)
				begin
				case contadorIterations is
					when "0000" =>
						Leds0 <= point;
						acerto <= point;
					when "0001" =>
						Leds1 <= point;
						acerto <= point;
					when "0010" =>
						Leds2 <= point;
						acerto <= point;
					when "0011" =>
						Leds3 <= point;
						acerto <= point;
					when "0100" =>
						Leds4 <= point;
						acerto <= point;
					when "0101" =>
						Leds5 <= point;
						acerto <= point;
					when "0110" =>
						Leds6 <= point;
						acerto <= point;
					when "1000" =>
						Leds7 <= point;
						acerto <= point;
					when "1001" =>
						Leds8 <= point;
						acerto <= point;
					when "1010" =>
						Leds9 <= point;
						acerto <= point;
					when others =>
						acerto <= point;
						Leds9 <= '0';
						Leds8 <= '0';
						Leds7 <= '0';
					   Leds6 <= '0';
						Leds5 <= '0';
					   Leds4 <= '0';
						Leds3 <= '0';
					   Leds2 <= '0';
						Leds1 <= '0';
						Leds0 <= '0';
				end case;
			end process;
			
			Leds <= Leds9 & Leds8 & Leds7 & Leds6 & Leds5 & Leds4 & Leds3 & Leds2 & Leds1 & Leds0;
			
			--=============================================ESTADO RESULT================================================================
			
			
			--Calcula o total de pontos 
			resultadoUltimo: muxPointsFinal port map(pontuacao, level,  calculoFinalPontos);
			mostraResultado: registradorOitoBits port map(Clk, enableResultReg, Reset,  calculoFinalPontos,  pontuacaoFinal);
			--Encontra as casas decimais das notas
			pontuacaoCasaZero: decod port map(pontuacaoFinal(3 downto 0), casaZeroNota);
			pontuacaoCasaUm: decod port map(pontuacaoFinal(7 downto 4), casaUmNota);
			
			--Mostra a nota final nos visores 
			notaFinal <= casaUmNota & casaZeroNota;
			
end game;
				
					
										
								
								
			
		
			
		