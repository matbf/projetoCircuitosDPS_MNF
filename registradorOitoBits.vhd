library IEEE;
use IEEE.Std_Logic_1164.all;
use ieee.std_logic_unsigned.all;

entity registradorOitoBits is 
	port (
			Clk: in std_logic;
			Enable: in std_logic;
			Reset: in std_logic;
			A: in std_logic_vector(7 downto 0);
			B: out std_logic_vector(7 downto 0)
			);
end registradorOitoBits;

architecture bhv of registradorOitoBits is
	begin
		process(Clk, Reset)
			begin
				if Reset = '0' then
					B <= "00000000";
				elsif Clk'Event and Clk = '0' then
					if Enable = '1' then 
						B <= A;
					end if;
				end if;
		end process;
end bhv;