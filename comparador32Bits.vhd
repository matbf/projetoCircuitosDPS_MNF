library IEEE;
use IEEE.Std_Logic_1164.all;

entity comparador32Bits is
			port(
					comparingConstant: in std_logic_vector(31 downto 0);
					comparingVariable: in std_logic_vector(31 downto 0);
					result: out std_logic
				 );
end comparador32Bits;

architecture behaviour of comparador32Bits is
		begin
			result <= '1' when comparingVariable = comparingConstant else
			'0';
end behaviour;
		