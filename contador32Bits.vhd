library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity contador32Bits is

port (
		Clk: in std_logic;
		Reset: in std_logic;
		Enable: in std_logic;
		limit: in std_logic_vector(31 downto 0);
		currentIteration: in std_logic_vector(31 downto 0);
		Iteration: out std_logic_vector(31 downto 0)
		);
end contador32Bits;

architecture timer of contador32Bits is
	begin 
		process(Clk, reset )
			begin	
				if reset = '0' then
					Iteration <= x"00000000";
	
				elsif Clk'event and Clk = '1' then
					if enable = '1' then
						if currentIteration > limit then
							Iteration <= x"00000000";
						else
							Iteration <= currentIteration + '1';
						end if;
					else
						Iteration <= currentIteration;
					end if;
				end if;
		end process;
end timer;