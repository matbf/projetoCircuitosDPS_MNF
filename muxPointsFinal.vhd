library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity muxPointsFinal is 
	port (
			pontos: in std_logic_vector( 3 downto 0);
			nivel: std_logic_vector( 1 downto 0);
			pontuacao: out std_logic_vector( 7 downto 0)
			);
end muxPointsFinal;


architecture resultado of muxPointsFinal is 
	signal A,B,C,D: std_logic_vector( 7 downto 0);

	begin

		A <= ("0000" & pontos);
		B <= ("000" & pontos) & "0";
		C <= ("00" & pontos) & "00";
		D <= ("0" & pontos) & "000";

 pontuacao <= A when nivel = "00" else
						B when nivel = "01" else
						C when nivel = "10" 
						else D;	
end resultado;