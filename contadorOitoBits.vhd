library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity contadorOitoBits is

port (
		Clk: in std_logic;
		Reset: in std_logic;
		enable: in std_logic;
		limit: in std_logic_vector(7 downto 0);
		currentIteration: in std_logic_vector(7 downto 0);
		Iteration: out std_logic_vector(7 downto 0)
		);
end contadorOitoBits;

architecture timer of contadorOitoBits is
	begin 
		process(enable, Reset)
			begin	
				if Reset'event and Reset = '0' then
					Iteration <= "00000000";
	
				elsif Clk'event and Clk = '1' then
					if enable = '1' then
						if currentIteration = limit then
							Iteration <= "00000000";
						else
							Iteration <= currentIteration + '1';
						end if;
					else
						Iteration <= currentIteration;
					end if;
				end if;
		end process;
end timer;