library IEEE;
use IEEE.Std_Logic_1164.all;


--A maquina consistira de 4 estados. No primeiro, o estado init, a maquina esperara o comando do usuario para iniciar o jogo (KEY(1))
--No segundo, o estado setup, a maquina esperara o gamer escolher qual nivel de velocidade ele jogara, e qual sequencia de numeros o jogo seguira, ele terminara o input apertando enter (KEY(1))
--No terceiro estado, o estado game, o jogo esperara o usuario enviar o comando de que inseriu o valor do jogo usando os SW(7 downto 0) com o KEY(1)
--No quarto estado, o estado results, os resultados da rodada serao computados, e caso seja a decima, o jogo parara. A nota do gamer sera 2 elevado ao nivel do jogo, vezes o numero de acertos

entity awesomeGame is
	port (
			CLOCK_50: in std_logic;
			KEY: in std_logic_vector(3 downto 0);   --KEY(0) zera a contagem alvos, reiniciando o jogo; 
															    --KEY(1) Enter (inicia o jogo no estado init; começa o jogo no estado setup; )
			SW: in std_logic_vector(9 downto 0);    --SW(9 downto 8) no estado setup escolhe o nivel do jogo;
															    --SW(1 downto 0) no estado setup, seleciona uma das memorias rom;
															    --SW(7 downto 0) no estado gaming, deve ser usada para indicar a codificaçao em binario os caracteres mostrados;
			HEX0: out std_logic_vector(6 downto 0); --Mostra a casa 0 de um hexadecimal a ser convertido pelo gamer;
			HEX1: out std_logic_vector(6 downto 0); --Mostra a casa 1 de um hexadecimal a ser convertido pelo gamer;
			HEX3: out std_logic_vector(6 downto 0); --Mostra a casa 0 da nota do gamer;
			HEX2: out std_logic_vector(6 downto 0); --Mostra a casa 1 da nota do gamer;
			HEX4: out std_logic_vector(6 downto 0); --Mostra o nivel escolhido pelo gamer;
			HEX5: out std_logic_vector(6 downto 0); --Mostra a letra L;
			LEDR: out std_logic_vector(9 downto 0)	 --Acende o enesimo led quando o usuario acerta o enesimo ponto( E.g.: Acertou somente o 3, entao LEDR <= '000000100');											
			
			);
			
end awesomeGame;

architecture fullGame of awesomeGame is
	signal enableSetup, enableResults, enableGame: std_logic;
	signal contaDez: std_logic;
	signal contaAteDez: std_logic;
	signal BTN0, BTN1, BTN2, BTN3, ResetDatapath: std_logic;
	signal numeroParaConversao, notaGamer: std_logic_vector(13 downto 0);
	
	component Datapath is
		port (
			enableSetupReg: in std_logic;
			enableGameReg: in std_logic;
			enableResultReg: in std_logic;
			Switches: in std_logic_vector(9 downto 0);
			Clk: in std_logic;
			Reset: in std_logic;
			numeroConversao: out std_logic_vector(13 downto 0);
			notaFinal: out std_logic_vector(13 downto 0); --Mostra a casa 1 de um hexadecimal a ser convertido pelo gamer;
			visorNivel: out std_logic_vector(6 downto 0);
			visorL: out std_logic_vector(6 downto 0);
			contaAteDez: out std_logic;
			Leds: out std_logic_vector(9 downto 0)--Mostra o ponto feito
			);
	end component;
	
	component ButtonSync is 
		port (
				KEY0, KEY1, KEY2, KEY3, CLK: in std_logic;
				BTN0, BTN1, BTN2, BTN3: out std_logic
				);
	end component;
	
	component controlador is
		port (
			--EA: in std_logic_vector(2 downto 0);
			Reset: in std_logic;
			ContaDez: in std_logic;
			Enter: in std_logic;
			--PE: out std_logic_vector(2 downto 0);
			Clk: in std_logic;
			EnableRegSetup: out std_logic;
			EnableRegResult: out std_logic;
			EnableRegGame: out std_logic;
			ResetDatapath: out std_logic
			);			
		end component;

	begin
	
		L1: ButtonSync port map (key(0), key(1), key(2), key(3), CLOCK_50, BTN0, BTN1, BTN2, BTN3);
		control: controlador port map(BTN0, contaDez, BTN1, CLOCK_50, enableSetup , enableResults, enableGame, ResetDatapath);
		passeData: datapath port map(enableSetup, enableGame, enableResults, SW, CLOCK_50, ResetDatapath, numeroParaConversao, notaGamer, HEX4, HEX5, contaAteDez, LEDR);
		HEX0 <= numeroParaConversao(6 downto 0);
		HEX1 <= numeroParaConversao(13 downto 7);
		HEX2 <= notaGamer(6 downto 0);
		HEX3 <= notaGamer(13 downto 7);
		contaDez <= contaAteDez;
		
end fullGame;	